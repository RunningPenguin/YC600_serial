/*
 * YC600_serial.h
 * Serial communications library for APSystems YC600 inverter.
 *
 * Version: 0.1
 * Author: Tobias Sachs (https://gitlab.com/RunningPenguin)
 */

#ifndef YC600_serial_h_
#define YC600_serial_h_

#include <Arduino.h>

//source https://gist.github.com/xxlukas42/fc0135639fcc1fc61c8b2a25649be59d
#include <EEPROM_Rotate.h>

#include <SoftwareSerial.h>

#include <stdio.h>
#include <string.h>

// #define DEBUG_CONCAT
// #define DEBUG_CRC
// #define DEBUG_BUILD_COMMANDS
// #define DEBUG_BUILD_ID
// #define DEBUG_CALCULATE_NUMBER
// #define DEBUG_INITIALIZE_CMD
// #define DEBUG_SERIAL_PRINT
// #define DEBUG_SERIAL_SENT
// #define DEBUG_SERIAL_RECEIVE
// #define DEBUG_DECODE_MESSAGE
// #define DEBUG_DECODE_MESSAGE_BUFFER
// #define DEBUG_DECODE_MESSAGE_DATA_EXTRACT
// #define DEBUG_DECODE_MESSAGE_DATA_EXTRACT_VALUE
// #define DEBUG_RECEIVED_VALUES
// #define DEBUG_BUILD_NORMAL_COMMANDS
// #define DEBUG_BUILD_NORMAL_COMMANDS_CONCAT
// #define DEBUG_BUILD_PAIRING_COMMANDS
// #define DEBUG_NORMAL_OPERATION_MODE
// #define DEBUG_EEPROM_READ_WRITE
// #define DEBUG_EEPROM_READ_WRITE_PAIRING
// #define DEBUG_PAIRING
// #define DEBUG_MODES
// #define DEBUG_CALCULATE_NUMBER_PAIRING_COMMANDS
// #define DEBUG_RESET_PAIRED

// #define DEBUG_OPERATION

// #define WRITE_PAIRING_FOR_DEBUG
// #define RESET_PAIRING_FOR_DEBUG

#if (defined DEBUG_CONCAT) || (defined DEBUG_CRC) || (defined DEBUG_BUILD_COMMANDS) || (defined DEBUG_BUILD_ID) || (defined DEBUG_CALCULATE_NUMBER) || (defined DEBUG_INITIALIZE_CMD) || (defined DEBUG_SERIAL_PRINT) || (defined DEBUG_SERIAL_SENT) || (defined DEBUG_SERIAL_RECEIVE) || (defined DEBUG_DECODE_MESSAGE) || (defined DEBUG_BUILD_NORMAL_COMMANDS) || (defined DEBUG_BUILD_NORMAL_COMMANDS_CONCAT) || (defined DEBUG_NORMAL_OPERATION_MODE) || (defined DEBUG_BUILD_PAIRING_COMMANDS) || (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT) || (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT_VALUE) || (defined DEBUG_RECEIVED_VALUES) || (defined DEBUG_EEPROM_READ_WRITE) || (defined DEBUG_DECODE_MESSAGE_BUFFER) || (defined DEBUG_EEPROM_READ_WRITE_PAIRING) || (defined DEBUG_PAIRING) || (defined DEBUG_MODES) || (defined DEBUG_CALCULATE_NUMBER_PAIRING_COMMANDS) || (defined DEBUG_OPERATION) || (defined WRITE_PAIRING_FOR_DEBUG) || (defined RESET_PAIRING_FOR_DEBUG) || (defined DEBUG_RESET_PAIRED)
#define DEBUG_SW_SERIAL
#endif

#ifndef YC600_MAX_NUMBER_OF_INVERTERS
#define YC600_MAX_NUMBER_OF_INVERTERS 5
#endif

#ifndef YC600_RX_PIN
#define YC600_RX_PIN 3 // RX pin connected to the CC2530
#endif
#ifndef YC600_TX_PIN
#define YC600_TX_PIN 1 // TX pin connected to the CC2530
#endif

#ifndef YC600_PIN_INVERSE
#define YC600_PIN_INVERSE 0 // Signal is inverted
#endif

#define YC600_SYNC_INTERVAL 5000 // Safe time between transmissions [ms]
#define YC600_BAUDRATE 115200    // UART baudrate of the CC2530 zigbee wireless interface to the YC600 inverter

// //For DEBUG reason only set interval very high so that normal operation doesn't crash during debug operation
// #define YC600_SYNC_INTERVAL 1000 // Safe time between transmissions [ms]
// #define YC600_SYNC_INTERVAL 16000000 // Safe time between transmissions [ms]

#ifndef CC2530_STARTUP_RESET_TIMES
#define CC2530_STARTUP_RESET_TIMES 3
#endif

#define CC2530_MAX_SERIAL_BUFFER_SIZE 512

#define EEPROM_SAFE_ADRESS 0

#define SENSOR_ERROR_OK 0           // No error
#define SENSOR_ERROR_OUT_OF_RANGE 1 // Result out of sensor range
#define SENSOR_ERROR_WARM_UP 2      // Sensor is warming-up
#define SENSOR_ERROR_TIMEOUT 3      // Response from sensor timed out
#define SENSOR_ERROR_UNKNOWN_ID 4   // Sensor did not report a known ID
#define SENSOR_ERROR_CRC 5          // Sensor data corrupted
#define SENSOR_ERROR_I2C 6          // Wrong or locked I2C address
#define SENSOR_ERROR_GPIO_USED 7    // The GPIO is already in use
#define SENSOR_ERROR_CALIBRATION 8  // Calibration error or Not calibrated
#define SENSOR_ERROR_OTHER 99       // Any other error

class YC600
{

public:
  //new from YC600 not in the CSE7766 available
  // void buildInitializeCommands(char EcuId[]);
  uint8_t calculateNumberOfInverters(char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20]);

  // ---------------------------------------------------------------------
  // Public
  // ---------------------------------------------------------------------

  YC600();
  virtual ~YC600();
  void setRxTx(unsigned char pin_rx, unsigned char pin_tx);
  void setInverted(bool inverted);
  unsigned char getRX();
  unsigned char getTX();
  bool getInverted();
  uint8_t getNumberOfSavedInverters(uint8_t MaxInverterNumber);
  bool getPaired(char EcuId[], char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20]);

  // possible values of getState() function call
  // == -1 we didn't found an inverter short ID by auto pair routine
  // == 10 reset of CC2530 detected
  // == 12 all inverters are paired at startup
  // == 13 not all inverters are paired at startup
  // == 16 we send a new string to the CC2530
  // == 18 we received a new string from the CC2530
  // == 20 we try to decode a new message
  // == 22 we successfully decoded a new message
  // == 30 we try to pair the actual inverter
  // == 34 we found an inverter short ID by auto pair routine
  // == 38 auto pairing is saving actual informations to EEPROM
  // == 40 auto pairing successfull saved actual informations to EEPROM
  // == 42 after auto pairing paired all inverters
  int8_t getState();

  // == -1 if no data was extracted since startup yet
  // == 0 for the first inverter from the inv_sns struct you gave to the begin function
  // == 1 for the second inverter from the inv_sns struct you gave to the begin function
  // == 2 ... and so on
  // should change every 5s (or if changed every YC600_SYNC_INTERVAL)
  // so if you have one inverter you should get new values from this one every 5s
  // if you have 2 inverters you get from each individual inverter updates every 10s
  // if you have 3 inverters each inverter updates at 15s
  // this has the reason that the polling from the Wemos D1 mini pro to the CC2530
  // and then over the air (ZigBee) to the YC600 and back takes some time
  // and we could only poll inverter at a time
  int8_t getUpdatedInverterNumber();

  void expectedCurrent(double expected);
  void expectedVoltage(unsigned int expected);
  void expectedPower(unsigned int expected);
  void expectedEnergy(unsigned int expected);
  void setCurrentRatio(double value);
  void setVoltageRatio(double value);
  void setPowerRatio(double value);
  void setEnergyRatio(double value);
  double getCurrentRatio();
  double getVoltageRatio();
  double getPowerRatio();
  double getEnergyRatio();
  void resetRatios();
  void resetEnergy(uint8_t inverterNumber, double value = 0);

  double getAcFrequency(uint8_t inverterNumber);         //AC frequency
  double getInverterTemperature(uint8_t inverterNumber); //temperature of the Inverter
  double getAcVoltage(uint8_t inverterNumber);           //AC voltage
  double getDcCurrentPanel1(uint8_t inverterNumber);     //DC Current Panel1
  double getDcCurrentPanel2(uint8_t inverterNumber);     //DC Current Panel2
  double getDcVoltagePanel1(uint8_t inverterNumber);
  double getDcVoltagePanel2(uint8_t inverterNumber);

  double getDcPowerPanel1(uint8_t inverterNumber);
  double getDcPowerPanel2(uint8_t inverterNumber);

  double getDcEnergyPanel1(uint8_t inverterNumber);
  double getDcEnergyPanel2(uint8_t inverterNumber);

  void setexpectedFrequency(double value);
  void setexpectedAcVoltage(double value);
  void setmaximumDcPanelCurrent(double value);
  void setmaximumDcPanelVoltage(double value);
  void setmaximumDcPanelPower(double value);

  void veryFirstBegin();
  void begin(char EcuId[], char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20]);
  String description();
  String description(unsigned char index);
  String address(unsigned char index);
  void tick();

protected:
  // ---------------------------------------------------------------------
  // protected
  // ---------------------------------------------------------------------

  int _pin_rx = YC600_RX_PIN;
  int _pin_tx = YC600_TX_PIN;
  bool _inverted = YC600_PIN_INVERSE;
  SoftwareSerial *_serial = NULL;

  float _frequency[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _temperature[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _acVoltage[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _currentPanel1[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _currentPanel2[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _voltagePanel1[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _voltagePanel2[YC600_MAX_NUMBER_OF_INVERTERS] = {0};

  float _p1[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _p2[YC600_MAX_NUMBER_OF_INVERTERS] = {0};

  float _t0[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _t1[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _t2[YC600_MAX_NUMBER_OF_INVERTERS] = {0};

  float _e10[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _e11[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _e12[YC600_MAX_NUMBER_OF_INVERTERS] = {0};

  float _e20[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _e21[YC600_MAX_NUMBER_OF_INVERTERS] = {0};
  float _e22[YC600_MAX_NUMBER_OF_INVERTERS] = {0};

  float _expectedFrequency = 50.0F;      // [Hz]
  float _expectedAcVoltage = 230.0F;     // [V]
  float _maximumDcPanelCurrent = 10.16F; // [A]
  float _maximumDcPanelVoltage = 33.69F; // [V]
  float _maximumDcPanelPower = 350.0F;   // [Wp] (Watts Peak)

  //***************************To Be deleted after change of all functions********//
  double _active = 0;
  double _reactive = 0;
  double _voltage = 0;
  double _current = 0;
  double _energy = 0;
  //***************************To Be deleted after change of all functions********//

  double _ratioV;
  double _ratioC;
  double _ratioP;
  double _ratioE;

  unsigned char _data[24] = {0}; //initialize everything as "0"

  char _ecu_id_reverse[254] = {0};     //initialize everything as "0"
  char _ecu_id_short[4] = {0};         //initialize everything as "0"
  char _reverse_ecu_id_short[4] = {0}; //initialize everything as "0"

  char _fullIncomingMessage[CC2530_MAX_SERIAL_BUFFER_SIZE] = {0}; //here we use an global buffer perhaps we could combine processIncomingByte and decodeMessage to avoid this

  char EEPROM_inv_ids_in[YC600_MAX_NUMBER_OF_INVERTERS][5] = {{0}};
  char EEPROM_inv_ids_out[YC600_MAX_NUMBER_OF_INVERTERS][5] = {{0}};

  char EEPROM_inv_sns_in[YC600_MAX_NUMBER_OF_INVERTERS][13] = {{0}};
  char EEPROM_inv_sns_out[YC600_MAX_NUMBER_OF_INVERTERS][13] = {{0}};

  char EEPROM_buffer_paired_in[YC600_MAX_NUMBER_OF_INVERTERS][6] = {{0}};
  char EEPROM_buffer_paired_out[YC600_MAX_NUMBER_OF_INVERTERS][6] = {{0}};

  char EEPROM_buffer_EcuId_out[YC600_MAX_NUMBER_OF_INVERTERS][13] = {{0}};

  char EEPROM_writeBufferString[YC600_MAX_NUMBER_OF_INVERTERS][254] = {{0}};

  char _tempInverterShortIdBuffer[6] = {0};
  char _tempSearchInverterLongIdBuffer[14] = {0};
  char _tempFoundInverterLongIdBuffer[14] = {0};

  int _error = 0;
  bool _dirty = true;
  bool _ready = false;

  bool _paired = false;
  bool _ready_to_send = true;
  bool _waiting_for_response = false;
  bool _message_decoded = false;
  bool _reset_done = false;
  bool _got_inverter_short_number = false;
  bool _pairing_done = false;
  bool _init_EEPROM_done = false;

  uint8_t _numberOfInverters = 0;
  uint8_t _numberOfPairedInverters = 0;
  uint8_t _tryingToPairInverterNumber = 0;
  uint8_t _loopcounterPairing = 0;

  uint8_t _local_paired = 0;

  int8_t _actualState = -1;

  //for the search of the inverter ID matching
  int8_t _YC600_index = -1;

  uint8_t _local_saved = 0;

  void _read();
  bool _serial_is_hardware();
  bool _serial_available();
  void _serial_flush();
  uint8_t _serial_read();

  double _defaultCurrentRatio(void);
  double _defaultVoltageRatio(void);
  double _defaultPowerRatio(void);
  double _defaultEnergyRatio(void);

  //new for YC600
  void _serial_print(char printString[]);
  void _getReverseId(char EcuId[]);
  void _getShortId(char EcuId[]);
  void _getReverseShortId(char EcuId[]);
  uint8_t _calculateNumberOfCommands(void);
  uint8_t _calculateNumberOfPairingCommands(void);
  void _buildCommand3(char EcuId[]);
  void _buildCommand5(char EcuId[]);
  void _buildCommand10(char EcuId[]);
  void _buildNormalOperationCommands(char EcuId[], char InvIds[YC600_MAX_NUMBER_OF_INVERTERS][8]); // I have no clue why I have to use more than 5 (6 is not enough too) just for the strncpy to do its work //sending of messages fail otherwise
  void _buildPairingCommands(char EcuId[], char InvSerial[YC600_MAX_NUMBER_OF_INVERTERS][20]);
  char *_sln(char Command[]);
  int _StrToHex(char str[]);
  char *_crc(char Command[]);
  void _buildInitializeCommands(char EcuId[]);

  void _initializeCC2530(bool Paired);
  void _operate(char EcuId[], bool Paired, char InvSerial[YC600_MAX_NUMBER_OF_INVERTERS][20]);

  void _processIncomingByte(const byte inByte);
  void _decodeMessage(char incomingMessage[CC2530_MAX_SERIAL_BUFFER_SIZE], char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20]);

  int _serial_availableForWrite();
  void _serial_write(int transmit);
  float _extractValue(uint8_t startPosition, uint8_t lengthOfValue, float slopeOfValue, float offsetOfValue, char incomingDecodeMessage[CC2530_MAX_SERIAL_BUFFER_SIZE]);

  void _writeString(String str, int pos);
  String _readString(int pos);
  float _checkRange(float testValue, float minimumValue, float maximumValue);

  bool _getPairedIndividualInverter(uint8_t inverterNumber, char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20], char EcuId[]);
  void _resetPaired();
  bool _writePaired(uint8_t inverterNumber, char InvSerial[14], char InverterShortId[6], char EcuId[], char Status[]);

  void _pairInverter(uint8_t inverterNumber, char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20]);
  char *_split(char *str, const char *delim);
  void _reset();
};

#endif /*YC600_serial_h_*/