# YC600

Library to pair and read out values from APSystems YC600 solar panel inverter. Tested with an Wemos D1 mini pro.
To work properly an TI CC2530/2531 board with a custom firmware has to be used. See "CC2530/2531-firmware" folder for this.
The library is written with easy access in mind. So the user only has to define which pins he use to communicate with the CC2530/2531. These should be real hardware serial pins, either 3,1 or 13,15 or (untested) by software serial the pin you would like to have.

## license
- https://creativecommons.org/licenses/by-nc-sa/3.0/ CC BY-NC-SA 3.0
- Use at your own risk
- everything is tested as good as I can but I couldn't give any guarantee
## Credits goes to:
- Discord Channel: https://discord.com/channels/801799706744717322/801799706744717325
- Github Discussion: https://github.com/Koenkk/zigbee2mqtt/issues/4221
- The Discord users: @kadzsol#5835 and @mujuli[DE]#5398 which developed the CC2530/2531 firmware
- The Discord user: @Skyscraper#8576 who helped with data extraction calculations
- I got some inspiration by: https://github.com/ingeniuske/CSE7766

## Feature list:
Tested (with only 2 inverters as I don't own more than these)
- Automatic pairing from 1-5 Inverters (more is also possible with only the change of one define)
- Saving all information's to EEPROM (Inverter serial number, from pairing routine extracted inverter short ID and ECU-ID)
- at startup comparing saved data from EEPROM to given parameters by "YC600.begin(ecu_id, inv_sns);" from "main.c" setup function
- if comparison says data isn't the same (e.g. Inverter serial number changed or ECU-ID changed) automatic pair again all YC600
- if comparison says data is the same go to normal mode and poll the YC600 one after the other
- not tested but implemented usage of other serial pins inclusive SW serial (only tested with pin 13/pin15)
- from outside of the library there are only two setup function "setRxTx" and "begin"
- from outside there is one "tick" function for the periodic loop
- there are 11 polling functions for the values individual for each YC600

Some untested stuff is there also:
- the polling functions make a range check and we could set new limits from outside of the library
- from outside of the library we could adapt to other "expected frequency" and "expected voltage" of the AC side of the inverter
- it should be possible to "calibrate" the readings with an factor from outside of the library
in the next Days I would comment everything a little bit clearer and build an homie MQTT Node from this: https://homieiot.github.io/

## Dependencies
- depends on https://github.com/xoseperez/eeprom_rotate Version 0.9.2

## flashing of CC2530
To flash the CC2530 I used this tutorial/method: https://zigbee.blakadder.com/flashing_ccloader.html
## Download

The Git repository contains the development version of these library. It is possible that there are some bugs.

## Expected output of Serial_ouptut.c example in the Software Serial Terminal application
### in normal operation mode (everything paired before)
You could see that in the first iteration we only get values for inverter 2 (InverterNumber 1) and after the second loop we get values for inverter 1 (InverterNumber 0).  
This is because we only pull one inverter every 5s and start right after the first decoding with the polling of data from outside the library and doesn't wait till the library polled every inverter once.
I let the timestamp in this example to give you an estimation on how long pairing and initialization took.

    10:23:34.801 ->
    10:23:34.801 ->
    10:23:34.801 -> **** Setup: initializing SW Serial outside ...
    10:23:34.801 ->
    10:23:35.133 -> **** actualState = all inverters are paired at startup
    10:23:44.792 -> **** actualState = reset of CC2530
    10:23:54.784 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:23:59.797 -> **** actualState = reset of CC2530
    10:24:04.809 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:24:49.847 -> **** actualState = we successfully decoded a new message
    10:24:49.847 -> **** InverterNumber = 1
    10:24:49.847 -> Decode: frequency: 50.01
    10:24:49.847 -> Decode: temperature: 15.95
    10:24:49.847 -> Decode: AC voltage: 228.00
    10:24:49.847 -> Decode: current Panel1: 1.56
    10:24:49.847 -> Decode: current Panel2: 1.57
    10:24:49.847 -> Decode: voltage Panel1: 33.86
    10:24:49.847 -> Decode: voltage Panel2: 33.88
    10:24:49.847 -> Decode: power Panel1: 38.67
    10:24:49.847 -> Decode: power Panel2: 38.88
    10:24:49.847 -> Decode: energy Panel1: 0.20
    10:24:49.880 -> Decode: energy Panel2: 0.20
    10:24:49.880 ->
    10:24:54.827 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:24:59.839 -> **** actualState = we successfully decoded a new message
    10:25:04.851 -> **** InverterNumber = 0
    10:25:04.851 -> Decode: frequency: 50.03
    10:25:04.851 -> Decode: temperature: 16.22
    10:25:04.851 -> Decode: AC voltage: 225.00
    10:25:04.851 -> Decode: current Panel1: 1.55
    10:25:04.851 -> Decode: current Panel2: 1.55
    10:25:04.851 -> Decode: voltage Panel1: 33.62
    10:25:04.851 -> Decode: voltage Panel2: 33\29
    10:25:04.851 -> Decode: power Panel1: 38.30
    10:25:04.851 -> Decode: power Panel2: 37.80
    10:25:04.851 -> Decode: energy Panel1: 0.19
    10:25:04.851 -> Decode: energy Panel2: 0.19
    10:25:04.851 ->
    10:25:09.830 -> **** InverterNumber = 1
    10:25:09.830 -> Decode: frequency: 49.99
    10:25:09.830 -> Decode: temperature: 15.95
    10:25:09.830 -> Decode: AC voltage: 227.00
    10:25:09.830 -> Decode: current Panel1: 1.55
    10:25:09.863 -> Decode: current Panel2: 1.57
    10:25:09.863 -> Decode: voltage Panel1: 33.98
    10:25:09.863 -> Decode: voltage Panel2: 33.90
    10:25:09.863 -> Decode: power Panel1: 49.87
    10:25:09.863 -> Decode: power Panel2: 48.20
    10:25:09.863 -> Decode: energy Panel1: 0.20
    10:25:09.863 -> Decode: energy Panel2: 0.20

### in Pairing Mode (new system, changed inverter serial numbers, changed ECU-ID)
The same as above is valid for the second inverter (InverterNumber 1) and the read out polling method. Here you see some messages with an "unknown string" e.g. at 10:41:03, 10:41:13 and 10:41:23 this is because the newly paired inverter did not respond so fast after a successfull pairing and sometimes need some time to settle down.

    10:38:48.211 -> **** Setup: initializing SW Serial outside ...
    10:38:48.211 ->
    10:38:48.509 -> **** actualState = not all inverters are paired at startup - need to pair
    10:38:58.200 -> **** actualState = reset of CC2530
    10:39:08.194 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:39:13.206 -> **** actualState = reset of CC2530
    10:39:18.218 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:39:48.226 -> **** actualState = auto pairing - we try to pair one inverter
    10:40:13.254 -> **** actualState = auto pairing - successfull saved inverter serial number, inverter short ID and ECU-ID to EEPROM
    10:40:18.233 -> **** actualState = auto pairing - we try to pair one inverter
    10:40:43.259 -> **** actualState = auto pairing - paired all inverters successfull
    10:40:48.272 -> **** actualState = all inverteps are paired at startup
    10:40:58.261 -> **** actualState = we successfully decoded a new message
    10:40:58.261 -> **** InverterNumber = 0
    10:40:58.261 -> Decode: frequency: 50.05
    10:40:58.261 -> Decode: temperature: 14.85
    10:40:58.261 -> Decode: AC voltage: 228.00
    10:40:58.261 -> Decode: current Panel1: 1.53
    10:40:58.261 -> Decode: current Panel2: 1.54
    10:40:58.261 -> Decode: voltage Panel1: 34.12
    10:40:58.261 -> Decode: voltage Panel2: 34.00
    10:40:58.261 -> Decode: power Panel1: 39.11
    10:40:58.261 -> Decode: power Panel2: 39.33
    10:40:58.294 -> Decode: energy Panel1: 0.21
    10:40:58.294 -> Decode: energy Panel2: 0.21
    10:40:58.294 ->
    10:41:03.241 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:41:08.253 -> **** actualState = we successfully decoded a new message
    10:41:13.233 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:41:18.244 -> **** actualState = we successfully decoded a new message
    10:41:23.256 -> **** actualState = we received a new string from the CC2530 - one or more unknown strings
    10:41:28.268 -> **** actualState = we successfully decoded a new message
    10:41:33.247 -> **** InverterNumber = 1
    10:41:33.247 -> Decode: frequency: 50.00
    10:41:33.247 -> Decode: temperature: 15.40
    10:41:33.280 -> Decode: AC voltage: 226.00
    10:41:33.280 -> Decode: current Panel1: 1.42
    10:41:33.280 -> Decode: current Panel2: 1.41
    10:41:33.280 -> Decode: voltage Panel1: 33.78
    10:41:33.280 -> Decode: voltage Panel2: 33.48
    10:41:33.280 -> Decode: power Panel1: 38.73
    10:41:33.280 -> Decode: power Panel2: 38.23
    10:41:33.280 -> Decode: energy Panel1: 0.21
    10:41:33.280 -> Decode: energy Panel2: 0.20
    10:41:33.280 ->
    10:41:38.259 -> **** InverterNumber = 0
    10:41:38.259 -> Decode: frequency: 50.03
    10:41:38.259 -> Decode: temperature: 14.85
    10:41:38.259 -> Decode: AC voltage: 228.00
    10:41:38.259 -> Decode: current Panel1: 1.42
    10:41:38.259 -> Decode: current Panel2: 1.42
    10:41:38.259 -> Decode: voltage Panel1: 33.98
    10:41:38.292 -> Decode: voltage Panel2: 34.06
    10:41:38.292 -> Decode: power Panel1: 45.71
    10:41:38.292 -> Decode: power Panel2: 45.71
    10:41:38.292 -> Decode: energy Panel1: 0.21
    10:41:38.292 -> Decode: energy Panel2: 0.21 