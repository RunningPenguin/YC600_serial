
#include "../include/YC600_serial.h"

//TODO
// return only values when we are in _operate_Mode
// use _split function more often to cut out the hard coded offset adresses
// after sucessful pairing run "begin" and resets all boolean/uint8_t underscore values back to initial values
// after sucessfull initialization increase Tick Time before first asking for Data

#ifdef DEBUG_SW_SERIAL
#include <SoftwareSerial.h>

SoftwareSerial DebugSwSer(14, 12); //Define hardware connections
#endif

EEPROM_Rotate EEPROMr;
#define DATA_OFFSET 42 //if you have issues with the EEPROM read only false values increase this number

char _EcuId[13] = {0};
char _InvIds[YC600_MAX_NUMBER_OF_INVERTERS][8] = {{0}};  // I have no clue why I have to use more than 5 (6 is not enough too) just for the strncpy to do its work //sending of messages fail otherwise
char _InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20] = {{0}}; // I have no clue why I have to use more than 5 (6 is not enough too) just for the strncpy to do its work //sending of messages fail otherwise

char _charPaired[] = "PAIRED";
char _deleted[] = "DELETE";

char CodedCommands[][254] =
    {
        "2605030103",
        "410000",       //reset
        "26050108FFFF", //append (concatenate) _ecu_id_reverse
        "2605870100",
        "26058302", //append (concatenate) _ecu_id_short
        "2605840400000100",
        "240014050F00010100020000150000",
        "2600",
        "6700",
        "2401FFFF1414060001000F1E",                        //append (concatenate) _ecu_id_reverse
        "FBFB1100000D6030FBD3000000000000000004010281FEFE" //append (concatenate) to line one above to combine to one string not used as single command only in combination with the above one
};

//lua version: for x=1, inv_num do
//lua version: 	ccmd[x]="2401"..string.sub(inv_id[x],3,4)..string.sub(inv_id[x],1,2).."1414060001000F13"..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2).."FBFB06BB000000000000C1FEFE"
//lua version: end
char normalOperationBaseCommand[][254] =
    {
        "2401",                      //+ ..string.sub(inv_id[x],3,4)..string.sub(inv_id[x],1,2)..
        "1414060001000F13",          //append (concatenate) _ecu_id_reverse
        "FBFB06BB000000000000C1FEFE" // end of String
};

char normalOperationInverterCommands[YC600_MAX_NUMBER_OF_INVERTERS][254] = {{0}}; //initialize everything as "0"

#define MAX_PAIRING_COMMANDS 5
char pairingOperationInverterCommands[YC600_MAX_NUMBER_OF_INVERTERS][MAX_PAIRING_COMMANDS][254] = {{{0}}}; //initialize everything as "0" //we have MAX_NUMBER_OF_INVERTERS by MAX 6 PAIRING commands and each with a maximum lenght of 254

/*  */ // -----------------------------------------------------------------------------
       // YC600 driver derived from:
       // CSE7766 based power monitor
       // Copyright (C) 2018 by Xose Pérez <xose dot perez at gmail dot com>
       // http://www.chipsea.com/UploadFiles/2017/08/11144342F01B5662.pdf
       // -----------------------------------------------------------------------------

// Constructor
YC600::YC600()
{
}
// Destructor
YC600::~YC600()
{
    if (_serial)
        delete _serial;
    //end();
}

// ---------------------------------------------------------------------

void YC600::setRxTx(unsigned char pin_rx, unsigned char pin_tx)
{
    Serial.flush(); //waits till everything above is sent out
    if (_pin_rx == pin_rx)
        return;
    _pin_rx = pin_rx;

    if (_pin_tx == pin_tx)
        return;
    _pin_tx = pin_tx;

    _dirty = true;
}

void YC600::setInverted(bool inverted)
{
    if (_inverted == inverted)
        return;
    _inverted = inverted;
    _dirty = true;
}

// ---------------------------------------------------------------------

unsigned char YC600::getRX()
{
    return _pin_rx;
}

unsigned char YC600::getTX()
{
    return _pin_tx;
}

bool YC600::getInverted()
{
    return _inverted;
}

char EEPROM_buffer_saved_paired_out[YC600_MAX_NUMBER_OF_INVERTERS][6] = {{0}};
uint8_t YC600::getNumberOfSavedInverters(uint8_t MaxInverterNumber)
{
#ifdef DEBUG_SW_SERIAL
    DebugSwSer.begin(115200); //Initialize software serial with baudrate of 115200

    while (!DebugSwSer)
        ;
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.println("**** Setup: initializing SW Serial ...");
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    _local_saved = 0;
#ifdef DEBUG_EEPROM_READ_WRITE
    DebugSwSer.println("++++ getNumberOfSavedInverters ");
    DebugSwSer.print("before begin _init_EEPROM_done: ");
    DebugSwSer.println(_init_EEPROM_done);
    DebugSwSer.println();
#endif

    if (_init_EEPROM_done == false)
    {
        veryFirstBegin();
    }

#ifdef DEBUG_EEPROM_READ_WRITE
    DebugSwSer.print("after begin _init_EEPROM_done: ");
    DebugSwSer.println(_init_EEPROM_done);
    DebugSwSer.println();
#endif

    for (uint8_t q = 0; q < MaxInverterNumber - 1; q++)
    {
        // Read data from flash memory (EEPROM library)
        String output_saved_string = _readString(EEPROM_SAFE_ADRESS + (40 * q));
        delayMicroseconds(250);                                                                //give memset a little bit of time to empty all the buffers
        memset(&EEPROM_buffer_saved_paired_out[0], 0, sizeof(EEPROM_buffer_saved_paired_out)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                                                                //give memset a little bit of time to empty all the buffers
        strncpy(EEPROM_buffer_saved_paired_out[q], output_saved_string.c_str() + 16, 6);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        if (strcmp(EEPROM_buffer_saved_paired_out[q], "PAIRED") == 0)
        {
            _local_saved++;
        }
    }
    return _local_saved;
}

bool YC600::getPaired(char EcuId[], char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20])
{
    for (uint8_t i = 0; i <= _numberOfInverters - 1; i++) //-1 because we start the array at 0
    {
        bool local_getPairedIndividualInverter = _getPairedIndividualInverter(i, InvSns, EcuId);

        if (local_getPairedIndividualInverter == false)
        {
#ifdef DEBUG_PAIRING
            DebugSwSer.print("_getPairedIndividualInverter(i, InvSns): ");
            DebugSwSer.println("We are not paired, trying to pair inverter!");
            DebugSwSer.print("*** inverterNumber = ");
            DebugSwSer.println(i);
            DebugSwSer.print("*** InvSns[i] = ");
            DebugSwSer.println(InvSns[i]);
            DebugSwSer.flush();
#endif
        }
        else
        {
#ifdef DEBUG_PAIRING
            DebugSwSer.println("*****DEBUG_PAIRING******** Inverter is paired!!!!!!!!");
            DebugSwSer.print("*** inverterNumber = ");
            DebugSwSer.println(i);
            DebugSwSer.flush();
#endif
            _numberOfPairedInverters++;
        }
    }

    if (_numberOfPairedInverters < _numberOfInverters)
    {
#if (defined DEBUG_PAIRING) || (defined DEBUG_OPERATION)
        DebugSwSer.println();
        DebugSwSer.print("only: ");
        DebugSwSer.print(_numberOfPairedInverters);
        DebugSwSer.print(" inverters out of: ");
        DebugSwSer.print(_numberOfInverters);
        DebugSwSer.println(" are paired!");
        DebugSwSer.println("trying to pair all again!");
        DebugSwSer.flush();
        delayMicroseconds(250);
#endif
        _numberOfPairedInverters = 0; // if we detect that one inverter isn't paired we would like to pair all inverters agains
        _paired = false;
        return false;
    }
    else
    {
        _actualState = 12; //all inverters are paired at startup
#ifdef DEBUG_OPERATION
        DebugSwSer.print("All ");
        DebugSwSer.print(_numberOfPairedInverters);
        DebugSwSer.println(" inverters are paired!");
        DebugSwSer.flush();
        delayMicroseconds(250);
#endif
        //rebuild NormalOperationCommands because we have other _InvIds now
        _buildNormalOperationCommands(_EcuId, _InvIds);
        _paired = true;
        return true;
    }
}

int8_t temp_actualState = -1;
int8_t YC600::getState()
{
    if (_actualState != -1)
    {
        temp_actualState = _actualState;
        _actualState = -1;
    }
    return temp_actualState;
}

int8_t YC600::getUpdatedInverterNumber()
{
    return _YC600_index;
}
// ---------------------------------------------------------------------

//function uses the global array inv_ids and calculates the number of the inverters
uint8_t YC600::calculateNumberOfInverters(char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20])
{
    //the use of _calculateNumberOfCommands(void) calculation didn't work because we change the array size dynamically
    uint8_t count = 0;
    //this for loop cycles through all content of the InvSns[] and looks if there is some content
    for (int i = 0; i < YC600_MAX_NUMBER_OF_INVERTERS; i++)
    {
#ifdef DEBUG_CALCULATE_NUMBER
        DebugSwSer.println();
        DebugSwSer.print("i: ");
        DebugSwSer.print(i);
        DebugSwSer.print("InvSns[i]");
        DebugSwSer.print(InvSns[i]);
        DebugSwSer.print("count: ");
        DebugSwSer.println(count);
        DebugSwSer.print("sizeof(InvSns): ");
        DebugSwSer.print(sizeof(InvSns));
        DebugSwSer.print("sizeof(InvSns[i]): ");
        DebugSwSer.println(sizeof(InvSns[i]));
        //DebugSwSer.print("strlen(InvSns): ");
        //DebugSwSer.print(strlen(InvSns));
        DebugSwSer.print("strcmp(InvSns[i], '0'): ");
        DebugSwSer.print(strcmp(InvSns[i], "0"));
        DebugSwSer.print("strlen(InvSns[i]): ");
        DebugSwSer.print(strlen(InvSns[i]));
        DebugSwSer.println();
        delay(500);
#endif
        if (strlen(InvSns[i]) > 3) //yes it's really dirty but works for this use case good enough all char arrays have more than 4 chars so its acceptable
        {
            count++;
        }
    }
#ifdef DEBUG_CALCULATE_NUMBER
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.print("count: ");
    DebugSwSer.print(count);
    DebugSwSer.println();
    DebugSwSer.println();
#endif
    _numberOfInverters = count;
    return _numberOfInverters;
}

// ---------------------------------------------------------------------

void YC600::expectedCurrent(double expected)
{
    if ((expected > 0) && (_currentPanel1[0] > 0))
    {
        _ratioC = _ratioC * (expected / _currentPanel1[0]); //only use the very first Panel on the first inverter for the calculation
    }
}

void YC600::expectedVoltage(unsigned int expected)
{
    if ((expected > 0) && (_voltagePanel1[0] > 0))
    {
        _ratioV = _ratioV * (expected / _voltagePanel1[0]);
    }
}

void YC600::expectedPower(unsigned int expected)
{
    if ((expected > 0) && (_p1[0] > 0))
    {
        _ratioP = _ratioP * (expected / _p1[0]);
    }
}

void YC600::expectedEnergy(unsigned int expected)
{
    if ((expected > 0) && (_e10[0] > 0))
    {
        _ratioE = _ratioE * (expected / _e10[0]);
    }
}

void YC600::setCurrentRatio(double value)
{
    _ratioC = value;
}

void YC600::setVoltageRatio(double value)
{
    _ratioV = value;
}

void YC600::setPowerRatio(double value)
{
    _ratioP = value;
}

void YC600::setEnergyRatio(double value)
{
    _ratioE = value;
}

double YC600::getCurrentRatio()
{
    return _ratioC;
}

double YC600::getVoltageRatio()
{
    return _ratioV;
}

double YC600::getPowerRatio()
{
    return _ratioP;
}

double YC600::getEnergyRatio()
{
    return _ratioE;
}

void YC600::resetRatios()
{
    _ratioC = _defaultCurrentRatio();
    _ratioV = _defaultVoltageRatio();
    _ratioP = _defaultPowerRatio();
    _ratioE = _defaultEnergyRatio();
}

void YC600::resetEnergy(uint8_t inverterNumber, double value)
{

    _e10[inverterNumber] = value;
    _e20[inverterNumber] = value;
}

void YC600::setexpectedFrequency(double value)
{
    _expectedFrequency = value;
}
void YC600::setexpectedAcVoltage(double value)
{
    _expectedAcVoltage = value;
}
void YC600::setmaximumDcPanelCurrent(double value)
{
    _maximumDcPanelCurrent = value;
}
void YC600::setmaximumDcPanelVoltage(double value)
{
    _maximumDcPanelVoltage = value;
}
void YC600::setmaximumDcPanelPower(double value)
{
    _maximumDcPanelPower = value;
}

double YC600::getAcFrequency(uint8_t inverterNumber)
{
    return _checkRange(_frequency[inverterNumber], _expectedFrequency - 5.0F, _expectedFrequency + 5.0F);
}

double YC600::getInverterTemperature(uint8_t inverterNumber)
{
    return _checkRange(_temperature[inverterNumber], -20.0F, 90.0F); //We need no changeable parameters here because the temperature doesn't depend on the panel/state in which we use the software
}

double YC600::getAcVoltage(uint8_t inverterNumber)
{
    return _checkRange(_acVoltage[inverterNumber], _expectedAcVoltage - 15.0F, _expectedAcVoltage + 15.0F);
}

double YC600::getDcCurrentPanel1(uint8_t inverterNumber)
{
    return _checkRange(_currentPanel1[inverterNumber] * _ratioC, 0.0F, _maximumDcPanelCurrent + 2.0F); // 2 Amps more than in the Datasheet
}

double YC600::getDcCurrentPanel2(uint8_t inverterNumber)
{
    return _checkRange(_currentPanel2[inverterNumber] * _ratioC, 0.0F, _maximumDcPanelCurrent + 2.0F); // 2 Amps more than in the Datasheet
}

double YC600::getDcVoltagePanel1(uint8_t inverterNumber)
{
    return _checkRange(_voltagePanel1[inverterNumber] * _ratioV, 0.0F, _maximumDcPanelVoltage + 4.0F); // 4 Volts more than in the Datasheet
}

double YC600::getDcVoltagePanel2(uint8_t inverterNumber)
{
    return _checkRange(_voltagePanel2[inverterNumber] * _ratioV, 0.0F, _maximumDcPanelVoltage + 4.0F); // 4 Volts more than in the Datasheet
}

double YC600::getDcPowerPanel1(uint8_t inverterNumber)
{
    return _checkRange(_p1[inverterNumber] * _ratioP, 0.0F, _maximumDcPanelPower + 10.0F); // 10 Watts more than in the Datasheet
}

double YC600::getDcPowerPanel2(uint8_t inverterNumber)
{
    return _checkRange(_p2[inverterNumber] * _ratioP, 0.0F, _maximumDcPanelPower + 10.0F); // ~0 Watts more than in the Datasheet
}

double YC600::getDcEnergyPanel1(uint8_t inverterNumber)
{
    return _checkRange(_e10[inverterNumber] * _ratioE, 0.0F, _maximumDcPanelPower * 14); // maximum 14h full sun in one day
}

double YC600::getDcEnergyPanel2(uint8_t inverterNumber)
{
    return _checkRange(_e20[inverterNumber] * _ratioE, 0.0F, _maximumDcPanelPower * 14); // maximum 14h full sun in one day
}

// ---------------------------------------------------------------------
// Sensor API
// ---------------------------------------------------------------------
void YC600::veryFirstBegin()
{
    //EEPROM emulation to save the bound ID from the inverters
    uint8_t size = 0;
    if (EEPROMr.last() > 1000)
    { // 4Mb boards
        size = 4;
    }
    else if (EEPROMr.last() > 250)
    { // 1Mb boards
        size = 2;
    }
    else
    {
        size = 1;
    }
    EEPROMr.size(size);
    EEPROMr.rotate(false); //disables EEPROM emulation rotation because of messing things up with spiffs if you don't use spiffs you could set these to true to have EEPROM rolling enabled
    EEPROMr.begin(4096);   // Initialising array of size 32 bytes in RAM to simulate the 32 bytes in Flash as EEPROM
    _init_EEPROM_done = true;

#ifdef WRITE_PAIRING_FOR_DEBUG
    DebugSwSer.print("** writePaired: ");
    char TEMP_ecu_id[] = "D8A3011B9880";
    char TEMP_shortinv_id[] = "3DC7";
    char TEMP_longinv_id[] = "408000112345";
    uint8_t TEMP_inv_ID = 0;
    _writePaired(TEMP_inv_ID, TEMP_longinv_id, TEMP_shortinv_id, TEMP_ecu_id, _charPaired);
    //TEMP_inv_ID = 1;
    //_writePaired(TEMP_inv_ID, TEMP_longinv_id, TEMP_shortinv_id, TEMP_ecu_id, _charPaired);
    //TEMP_inv_ID = 2;
    //_writePaired(TEMP_inv_ID, TEMP_longinv_id, TEMP_shortinv_id, TEMP_ecu_id, _charPaired);
    _tryingToPairInverterNumber = 0; // if we detect that one inverter isn't paired we would like to pair all inverters agains and start with the first one index=0
    delay(4000);
#endif
#ifdef DEBUG_EEPROM_READ_WRITE_PAIRING
    DebugSwSer.println("**** veryFirstBegin");
    DebugSwSer.print("** _init_EEPROM_done: ");
    DebugSwSer.println(_init_EEPROM_done);
    DebugSwSer.println();
    String output_string = _readString(EEPROM_SAFE_ADRESS + (40 * 0)); //0 is inverter ID
    DebugSwSer.print("** output_string1: ");
    DebugSwSer.println(output_string);
    output_string = _readString(EEPROM_SAFE_ADRESS + (40 * 1)); //1 is inverter ID
    DebugSwSer.print("** output_string2: ");
    DebugSwSer.println(output_string);
    DebugSwSer.println();
    DebugSwSer.println();
    delay(4000);
#endif
#ifdef DEBUG_OPERATION
    String output_string = _readString(EEPROM_SAFE_ADRESS + (40 * 0)); //0 is inverter ID
    DebugSwSer.println("** Saved inverter pairing information's in EEPROM: ");
    delayMicroseconds(400);
    for (uint8_t i = 0; i < YC600_MAX_NUMBER_OF_INVERTERS; i++)
    {
        output_string = _readString(EEPROM_SAFE_ADRESS + (40 * i)); //1 is inverter ID
        delayMicroseconds(400);
        DebugSwSer.printf("** inverternumber %u: ", i);
        DebugSwSer.println(output_string);
        delayMicroseconds(400);
    }
    DebugSwSer.flush();
    delayMicroseconds(400);
#endif
#ifdef RESET_PAIRING_FOR_DEBUG
    _resetPaired();
    DebugSwSer.println("**** veryFirstBegin");
    DebugSwSer.print("** _init_EEPROM_done: ");
    DebugSwSer.println(_init_EEPROM_done);
    DebugSwSer.println();
    String output_string = _readString(EEPROM_SAFE_ADRESS + (40 * 0)); //0 is inverter ID
    DebugSwSer.print("** output_string1: ");
    DebugSwSer.println(output_string);
    for (uint8_t i = 0; i < YC600_MAX_NUMBER_OF_INVERTERS; i++)
    {
        output_string = _readString(EEPROM_SAFE_ADRESS + (40 * i)); //1 is inverter ID
        DebugSwSer.printf("** output_string%u: ", i);
        DebugSwSer.println(output_string);
    }
    DebugSwSer.println();
    DebugSwSer.println();
    delay(4000);
#endif
}

// Initialization method, must be idempotent
void YC600::begin(char EcuId[], char InvSns[][20])
{
    resetRatios();
    if (!_dirty)
        return;

    if (_serial)
        delete _serial;

    if (3 == _pin_rx)
    {
        Serial.begin(YC600_BAUDRATE);
        while (!Serial)
            ;
    }
    else if (13 == _pin_rx)
    {
        Serial.begin(YC600_BAUDRATE);
        while (!Serial)
            ;
        Serial.flush();
        Serial.swap();
    }
    else
    {
        _serial = new SoftwareSerial(_pin_rx, _pin_tx, _inverted); //TODO to use software serial with YC600 TX has to be enabled
        _serial->enableIntTx(false);                               //TODO to use software serial with YC600 TX has to be enabled
        _serial->begin(YC600_BAUDRATE);
    }

    if (_init_EEPROM_done == false)
    {
        veryFirstBegin();
    }

    memset(&_EcuId[0], 0, sizeof(_EcuId));   //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                  //give memset a little bit of time to empty all the buffers
    memset(&_InvSns[0], 0, sizeof(_InvSns)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                  //give memset a little bit of time to empty all the buffers
    memset(&_InvIds[0], 0, sizeof(_InvIds)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                  //give memset a little bit of time to empty all the buffers

    strncpy(_EcuId, EcuId, strlen(EcuId));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

    _getReverseId(_EcuId);

    //old calculateNumberOfInverters(*InvSns);
    calculateNumberOfInverters(InvSns);

    //Reads out EEPROM, compares EEPROM to given EcuId and InvSns if both match copy the EEPROM values in the "underscore" variables
    bool _localGetPaired = getPaired(EcuId, InvSns);

    if (_localGetPaired == false)
    {
        for (uint8_t i = 0; i <= _numberOfInverters - 1; i++) //-1 because we start the array at 0
        {

            strncpy(_InvSns[i], InvSns[i], 14); //strlen(InvSns[i]));     //in this case we don't have the right InvSns in the EEPROM
            delayMicroseconds(250);             //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_OPERATION
            DebugSwSer.print("The given serials doesn't match to the saved ones, we have to pair! ");
            DebugSwSer.println();
            DebugSwSer.flush();
#endif
#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("copying serial Numbers from given number not from EEPROM: ");
            DebugSwSer.print(i);
            DebugSwSer.print(" InvSns[i]: ");
            DebugSwSer.println(InvSns[i]);
            DebugSwSer.print(" _InvSns[i]: ");
            DebugSwSer.println(_InvSns[i]);
            DebugSwSer.flush();
            delayMicroseconds(250);
#endif
        }
        _actualState = 13; //not all inverters are paired at startup
        _resetPaired();    //to make it possible to decrease the number of paired inverters
    }

    _buildInitializeCommands(_EcuId);

    _buildPairingCommands(_EcuId, _InvSns); //For this we have to use the InvSns we got from the function not the saved ones

    _buildNormalOperationCommands(_EcuId, _InvIds);

#ifdef DEBUG_BUILD_PAIRING_COMMANDS
    DebugSwSer.println();
    DebugSwSer.println("DEBUG_BUILD_PAIRING_COMMANDS: ");
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    _dirty = false;
}

// Descriptive name of the sensor
String YC600::description()
{
    char buffer[28];
    if (_serial_is_hardware())
    {
        snprintf(buffer, sizeof(buffer), "YC600_BAUDRATE @ HwSerial");
    }
    else
    {
        snprintf(buffer, sizeof(buffer), "YC600_BAUDRATE @ DebugSwSerial(%u,NULL)", _pin_rx);
    }
    return String(buffer);
}

// Descriptive name of the slot # index
String YC600::description(unsigned char index)
{
    return description();
};

// Address of the sensor (it could be the GPIO or I2C address)
String YC600::address(unsigned char index)
{
    return String(_pin_rx);
}

// Loop-like method, call it in your main loop
unsigned long lastTick = millis(); //don't know why static is such a problem in C++
void YC600::tick()
{

    //static unsigned long lastTick = millis(); //don't know why static is such a problem in C++

#if (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT) || (defined DEBUG_DECODE_MESSAGE)

    // First inverter answer to extract inverter short ID
    //    _pairInverter(0, _InvSns);
    //    _pairInverter(1, _InvSns);
    //    // have to match - puts the following line on the DEBUG serial
    //    // Found Inverter short ID: D021 with serial number: 408000118145
    //    // Decode: found inverter: D021
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("first Test String have to find inverter");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE1A448100000C0200001414014E0067D0030000064080001181453A7C0D72FE1C44810000010121D01414006B0038D303000008FF1A40800011814521D00EAC", _InvSns);
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("first Test String have to find inverter");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE1A448100000C02000014140171006CD00300000640800011814521D00DF1FE1C44810000010121D014140071006AD303000008FF1A40800011814521D00EE4", _InvSns);
    //
    //    // doesn't match no further messsages in DEBUG serial with "Decode: found inverter:" until the message "back to normal!" appears!!!
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("next Test String is not allowed to find inverter!");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0E670000FFFF80971B01A3D800000709001145C0088CFE0145C0088CFE0145C0088CFE0145C0088CFE0145C0088CFE0145C0098D", _InvSns);
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("next Test String is not allowed to find inverter!");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE25448100000D0200001414017300C46E03000011408000118145FFFF10FFFF80971B01A3D821D00DAA0145C0098D", _InvSns);
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("next Test String is not allowed to find inverter!");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE25448100000F0100001414017100313204000011408000118145A3D810FFFF80971B01A3D821D00D7CFE1C44810000010121D01414007100AB3804000008FFFF40800011814521D00E2C", _InvSns);
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("next Test String is not allowed to find inverter!");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE1A44810000010100001414016300D6930400000680971B01A3D83A7C0DC7FF80971B01A3D821D00D7CFE1C44810000010121D01414007100AB3804000008FFFF40800011814521D00E2C", _InvSns);
    //    delay(2000);
    //#ifdef DEBUG_DECODE_MESSAGE
    //    DebugSwSer.println();
    //    DebugSwSer.println("next Test String is not allowed to find inverter!");
    //    DebugSwSer.println();
    //#endif
    //    _decodeMessage((char *)"FE0164020067FE1A44810000010100001414016900D4930400000680971B01A3D83A7C0DCFFF80971B01A3D821D00D79FE1C44810000010121D01414006100A13904000008FFFF40800011814521D00E37", _InvSns);
    // only test Data receive strings from two of my Inverters
    delay(2000);
#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.println();
    DebugSwSer.println("next Test String (normal operation) should give back some data from an inverter!");
    DebugSwSer.println();
#endif
    _decodeMessage((char *)"FE0164010064FE034480001401D2FE0345C43A7C00C4FE724481000006013A7C141400540001950400005E408000119348FBFB51B103E20F425800005007000000A51068A5F06604D30000000000000000F51A337300F8029AF01F00030558073F0303030100000100000000000000000000000000000000000000000000000000000000000000FEFE3A7C0E11", _InvSns);
    delay(2000);
#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.println();
    DebugSwSer.println("next Test String (normal operation) should give back some data from an inverter!");
    DebugSwSer.println();
#endif
    _decodeMessage((char *)"FE0164010064FE034480001401D2FE0345C421D00073FE7244810000060121D01414005C0043DE0400005E408000118145FBFB51B103DF0F433000005037000000A7B068A8406704CE0000000000000000FE40680E00FBCF92551F00030550073F0303030100000100000000000000000000000000000000000000000000000000000000000000FEFE21D00E74", _InvSns);

    // New Test Strings only test Data receive strings from two of my Inverters
    delay(2000);
#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.println();
    DebugSwSer.println("next Test String (normal operation) should give back some data from an inverter!");
    DebugSwSer.println();
#endif
    _decodeMessage((char *)"FE0164010064FE034480001401D2FE724481000006013A7C141400460023DF0600005E408000119348FBFB51B1042A0F430C0000819600000000C263FE816304D30000000000000006C241C88306CF105F3C1F0003055D073F0303030100000100000000000000000000000000000000000000000000000000000000000000FEFE3A7C0EAE", _InvSns);
    delay(2000);
#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.println();
    DebugSwSer.println("next Test String (normal operation) should give back some data from an inverter!");
    DebugSwSer.println();
#endif
    _decodeMessage((char *)"FE0164010064FE034480001401D2FE7244810000060121D01414007B00B7400700005E408000118145FBFB51B104300F4440000081BF000000FBF163FDB16304DA0000000000000006DF35E87F06DDBCCE0E1F00030553073F0303030100000100000000000000000000000000000000000000000000000000000000000000FEFE21D00EAA", _InvSns);

    delay(2000);
#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.println();
    DebugSwSer.println("###############################back to normal!#########################################################");
    DebugSwSer.println();
#endif
#endif

    if (millis() - lastTick > YC600_SYNC_INTERVAL)
    {
        lastTick = millis(); //maybe synchronize sending and reading (reset the read last variable at the beginning of _waiting_for_response=true)

#ifdef DEBUG_MODES
        DebugSwSer.println();
        DebugSwSer.println("#########################TICK");
        DebugSwSer.print("_paired = ");
        DebugSwSer.println(_paired);
        DebugSwSer.print("_waiting_for_response = ");
        DebugSwSer.println(_waiting_for_response);
        DebugSwSer.print("_ready = ");
        DebugSwSer.println(_ready);
        DebugSwSer.print("_ready_to_send = ");
        DebugSwSer.println(_ready_to_send);
        DebugSwSer.print("_pairing_done");
        DebugSwSer.println(_pairing_done);
        DebugSwSer.print("_got_inverter_short_number");
        DebugSwSer.println(_got_inverter_short_number);
        //DebugSwSer.print("_got_inverter_short_number");
        //DebugSwSer.println(_got_inverter_short_number);
        //DebugSwSer.print("_got_inverter_short_number");
        //DebugSwSer.println(_got_inverter_short_number);
        //DebugSwSer.print("_got_inverter_short_number");
        //DebugSwSer.println(_got_inverter_short_number);
        //DebugSwSer.print("_got_inverter_short_number");
        //DebugSwSer.println(_got_inverter_short_number);
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        if (_waiting_for_response == true) //in this order we send directly after we received/decoded one message if we need a short waiting between decoding and sending exchange the order here
        {
#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("*** READING_MODE: ");
            DebugSwSer.flush();
#endif
            _read();
        }

        if (_paired == false && _ready == false)
        {
#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("*** PAIRING_MODE_INITALIZE: ");
            DebugSwSer.flush();
#endif
            _initializeCC2530(_paired);
        }
        else if (_paired == false && _ready == true)
        {

#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("*** PAIRING_MODE_PAIRING: ");
            DebugSwSer.flush();
#endif

            if (_got_inverter_short_number == true && _pairing_done == true) //in this order we send directly after we received/decoded one message if we need a short waiting between decoding and sending exchange the order here
            {
#ifdef DEBUG_MODES
                DebugSwSer.println();
                DebugSwSer.println("*** SAVE_PAIRING_MODE: ");
                DebugSwSer.flush();
#endif
#ifdef DEBUG_PAIRING
                DebugSwSer.print("_tempInverterShortIdBuffer: ");
                DebugSwSer.print(_tempInverterShortIdBuffer);
                DebugSwSer.print(" _tempFoundInverterLongIdBuffer: ");
                DebugSwSer.print(_tempFoundInverterLongIdBuffer);
                DebugSwSer.print(" _InvSns[_tryingToPairInverterNumber]: ");
                DebugSwSer.println(_InvSns[_tryingToPairInverterNumber]);
                DebugSwSer.flush();
#endif
                if (strcmp(_tempFoundInverterLongIdBuffer, _InvSns[_tryingToPairInverterNumber]) == 0)
                {
                    if (_writePaired(_tryingToPairInverterNumber, _tempFoundInverterLongIdBuffer, _tempInverterShortIdBuffer, _EcuId, _charPaired))
                    {
                        _actualState = 40; //we saved pairing informations of the actual inverter
                        _pairing_done = false;
                    }
                    else
                    {
                        _actualState = 42; //all inverters got paired by auto pairing function
#ifdef DEBUG_OPERATION
                        DebugSwSer.println();
                        DebugSwSer.println("*** ALL Inverters are now paired, perhaps we should reset status and start from beginning: ");
                        DebugSwSer.flush();
                        delayMicroseconds(250);
#endif
                        //we don't need this functions as the transition between pairing and operation mode is working
                        //_reset();
                        //begin(_EcuId, _InvSns);
                    }
                }
            }
            else if (getPaired(_EcuId, _InvSns) == false)
            {
                _pairInverter(_tryingToPairInverterNumber, _InvSns);
            }
        }
        else if (_ready_to_send == true && _paired == true && _pairing_done == false && _ready == false) //in this order we send directly after we received/decoded one message if we need a short waiting between decoding and sending exchange the order here
        {
#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("*** INITIALIZE_MODE: ");
            DebugSwSer.flush();
#endif
            _initializeCC2530(_paired);
        }
        else if (_ready == true && _paired == true)
        {
#ifdef DEBUG_MODES
            DebugSwSer.println();
            DebugSwSer.println("*** NORMAL_OPERATION_MODE: ");
            DebugSwSer.flush();
#endif
            //normal operation
            _operate(_EcuId, _paired, _InvSns);
        }
    }
}

// ---------------------------------------------------------------------
// Protected
// ---------------------------------------------------------------------

char oneChar[10] = {0};
//processes the incoming message and stores it ready for decode
void YC600::_processIncomingByte(const byte inByte)
{

    sprintf(oneChar, "%02x", inByte);
    strncat(_fullIncomingMessage, oneChar, 2);

#ifdef DEBUG_SERIAL_RECEIVE
    DebugSwSer.print("Receive: ");
    DebugSwSer.printf("%02x", inByte);
    DebugSwSer.println();
    DebugSwSer.print("oneChar: ");
    DebugSwSer.println(oneChar);
    DebugSwSer.print("_fullIncomingMessage: ");
    DebugSwSer.println(_fullIncomingMessage);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

} // end of processIncomingByte

char messageToDecode[CC2530_MAX_SERIAL_BUFFER_SIZE] = {0};
char tempMsgBuffer[254] = {0};
char _resetString[26] = "FE064180020202020702C2";
char _CC2530_answer_string[] = "44810000";
char _noAnswerFromInverter[32] = "FE0164010064FE034480CD14011F";
char _s_l_string[14] = {0};
int _s_l = 0;
char _s_d[254] = {0};
char _i_i[254] = {0};

uint8_t Message_begin_offset = 0;
float YC600::_extractValue(uint8_t startPosition, uint8_t lengthOfValue, float slopeOfValue, float offsetOfValue, char incomingDecodeMessage[CC2530_MAX_SERIAL_BUFFER_SIZE])
{
#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT_VALUE
    DebugSwSer.println();
    DebugSwSer.println("++++++ _extractValue: ");
    DebugSwSer.print("startPosition: ");
    DebugSwSer.println(startPosition);
    DebugSwSer.print("lengthOfValue: ");
    DebugSwSer.println(lengthOfValue);
    DebugSwSer.print("slopeOfValue: ");
    DebugSwSer.println(slopeOfValue);
    DebugSwSer.print("offsetOfValue: ");
    DebugSwSer.println(offsetOfValue);
    DebugSwSer.print("incomingDecodeMessage: ");
    DebugSwSer.println(incomingDecodeMessage);
    DebugSwSer.println();
#endif

    memset(&tempMsgBuffer[0], 0, sizeof(tempMsgBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                              //give memset a little bit of time to empty all the buffers
    strncpy(tempMsgBuffer, incomingDecodeMessage + startPosition, lengthOfValue);
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT_VALUE
    DebugSwSer.println();
    DebugSwSer.print("tempMsgBuffer: ");
    DebugSwSer.println(tempMsgBuffer);
    DebugSwSer.println();
    DebugSwSer.println("------ _extractValue: ");
    DebugSwSer.println();
#endif

    return (slopeOfValue * (float)_StrToHex(tempMsgBuffer)) + offsetOfValue;
}

//Float to Int with round to nearest
#define FLOAT_TO_INT(x) ((x) >= 0 ? (int)((x) + 0.5) : (int)((x)-0.5))

char tempDecodeBuffer[254] = {0};
//decodes the full message
void YC600::_decodeMessage(char incomingMessage[CC2530_MAX_SERIAL_BUFFER_SIZE], char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20])
{

    strncpy(messageToDecode, incomingMessage, strlen(incomingMessage));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT
    DebugSwSer.print("***** Received Data in _decodeMessage: ");
    DebugSwSer.print("messageToDecode = ");
    DebugSwSer.print(messageToDecode);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    _actualState = 18; //we received a new string from the CC2530
#ifdef DEBUG_OPERATION
    DebugSwSer.print("Received: ");
    DebugSwSer.println(messageToDecode);
    DebugSwSer.flush();
    delayMicroseconds(250);
#endif

    if (strlen(messageToDecode) > 222)
    {
        _actualState = 20; //we try to decode a new message
        char *tail;

        tail = _split(messageToDecode, _CC2530_answer_string);
#ifdef DEBUG_DECODE_MESSAGE
        if (tail)
        {
            DebugSwSer.printf("head: '%s'\n", messageToDecode);
            DebugSwSer.printf("tail: '%s'\n", tail);
            DebugSwSer.flush();
        }
#endif
        Message_begin_offset = 28;
        memset(&_s_l_string[0], 0, sizeof(_s_l_string)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                          //give memset a little bit of time to empty all the buffers
        strncpy(_s_l_string, messageToDecode + 2 + Message_begin_offset, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        _s_l = _StrToHex(_s_l_string);

        memset(&_s_d[0], 0, sizeof(_s_d)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);            //give memset a little bit of time to empty all the buffers
        strncpy(_s_d, messageToDecode + 42 + Message_begin_offset, 2 * _s_l + 1);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        //NEW VERSION of _s_d generation with search string
        memset(&_s_d[0], 0, sizeof(_s_d)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);            //give memset a little bit of time to empty all the buffers
        strncpy(_s_d, tail + 30, strlen(tail));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        memset(&_i_i[0], 0, sizeof(_i_i)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);            //give memset a little bit of time to empty all the buffers
        strncpy(_i_i, _s_d, 11);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_DECODE_MESSAGE
        DebugSwSer.println();
        DebugSwSer.print("Decode: _s_l: ");
        DebugSwSer.println(_s_l);
        DebugSwSer.print("Decode: _s_d: ");
        DebugSwSer.println(_s_d);
        DebugSwSer.print("Decode: _i_i: ");
        DebugSwSer.println(_i_i);
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        for (uint8_t r = 0; r < YC600_MAX_NUMBER_OF_INVERTERS; r++)
        {
#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT
            DebugSwSer.print("Decode: InvSns[r]: ");
            DebugSwSer.print(InvSns[r]);
#endif
            if (strstr(InvSns[r], _i_i) != NULL)
            {
#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT
                DebugSwSer.printf("Found '%s' in position:\t %d\n", _i_i, r);
                DebugSwSer.flush();
#endif
                _YC600_index = r;
            }
        }

        if (strcmp(messageToDecode, _noAnswerFromInverter) != 0)
        {

#ifdef DEBUG_DECODE_MESSAGE_DATA_EXTRACT
            DebugSwSer.println();
            DebugSwSer.print("**** messageToDecode: ");
            DebugSwSer.print(messageToDecode);
            DebugSwSer.println();
#endif

            _frequency[_YC600_index] = (float)50000000 / _extractValue(24, 6, 1, 0, _s_d); //[Hz]

            // By hand calculation according to Ghidra reverse engineered original Code with Test String 1 from decode Test
            // from Discord temp 03E2 -> 0x03*256 + 0xE2*330 = 3*256+226*330 = 768+74580 = 75348 -> 75348 +- 245760 = -170412 (if this value is < 0, then do: 75348 +- 4095 = 71253) - and THEN do a bitshift >> 12 -> Result: 17.39°C
            // in Python: var1 = data[0] * 256 + data[1] * 330
            // in Python:         var2 = var1 + -245760
            // in Python:         if var2 < 0:
            // in Python:             var2 = var1 + -4095
            // in Python:         temperature = var2 >> 12
            // _temperature[_YC600_index] = (_extractValue(20, 2, 256.0F, 0, _s_d) + _extractValue(22, 2, 330.0F, 0, _s_d)); //[°C]
            // if (_temperature[_YC600_index] + (float)-245760 < 0)
            // {
            //     _temperature[_YC600_index] = _temperature[_YC600_index] + (float)-4095;
            // }
            // _temperature[_YC600_index] = _temperature[_YC600_index] / (float)4096;

            // From reverse engineering
            _temperature[_YC600_index] = _extractValue(20, 4, 0.2752F, -258.7F, _s_d); //[°C]

            // By hand calculation according to Ghidra reverse engineered original Code with Test String 1 from decode Test
            // 04D3 <- aV = 0x4D3 = 1235 / 1,3277 = 930,18... -> ROUND -> 930 -> Use only first 8 bits (930 in binary: 1110100010 -> 11101000 -> back to decimal: 232 V
            //_acVoltage[_YC600_index] = _extractValue(56, 4, 0.1873F, 0, _s_d);             //[V]
            //_acVoltage[_YC600_index] = (float)((uint32_t)(_extractValue(56, 4, 1, 0, _s_d)*((float)1/(float)1.3277))&0xFF00);             //[V]
            //_acVoltage[_YC600_index] = (uint16_t)(_extractValue(56, 4, 1, 0, _s_d)*((float)1/(float)1.3277))>>2;
            //same as above but with a little bit more precision as we round to the nearest und not to floor
            _acVoltage[_YC600_index] = (float)((_extractValue(56, 4, 1, 0, _s_d) * ((float)1 / (float)1.3277)) / 4);

            // memset(&tempDecodeBuffer[0], 0, sizeof(tempDecodeBuffer));                                        //zero out all buffers we could work with "messageToDecode"
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // strncpy(tempDecodeBuffer, _s_d + 47, 1);                                                          //first byte of current Panel1
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // strncat(tempDecodeBuffer, _s_d + 44, 2);                                                          //second+third byte of current Panel1
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // #ifdef DEBUG_DECODE_MESSAGE_BUFFER
            //             DebugSwSer.print("**** DEBUG_DECODE_MESSAGE_BUFFER = ");
            //             DebugSwSer.print(_YC600_index);
            //             DebugSwSer.println(" Current: ");
            //             DebugSwSer.print("tempDecodeBuffer: ");
            //             DebugSwSer.print(tempDecodeBuffer);
            //             DebugSwSer.println();
            //             DebugSwSer.println();
            //             DebugSwSer.flush();
            // #endif
            // _currentPanel1[_YC600_index] = _extractValue(0, 3, ((float)27.5 / (float)4096), 0, tempDecodeBuffer); //[A]
            //same as above in one line:
            _currentPanel1[_YC600_index] = (_extractValue(47, 1, (float)256, 0, _s_d) + _extractValue(44, 2, 1, 0, _s_d)) * (float)27.5 / (float)4096; //[A]

            // By hand calculation according to Ghidra reverse engineered original Code with Test String 1 from decode Test
            // A51068 <- dI1/dV1 - dV1 = (0x68*16 + 0x1) = (104*16 + 1) = (1664+1) * 82,5/4096 = 33,5357666015625 V - => dI1 = 0x0*256 + 0xA5 = (0*256 + 165) = (165)27,5/4096 = 1,1077880859375 A
            // A5F066 <- dI2/dV2 - dV2 = (0x66*16 + 0xF) = (102*16 + 15) = (1632+15) * 82,5/4096 = 33,1732177734375 V - => dI2 = 0x0*256 + 0xA5 = (0256 + 165)= (165)*27,5/4096 = 1,1077880859375 A

            // memset(&tempDecodeBuffer[0], 0, sizeof(tempDecodeBuffer));                                        //zero out all buffers we could work with "messageToDecode"
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // strncpy(tempDecodeBuffer, _s_d + 53, 1);                                                          //first byte of current Panel2
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // strncat(tempDecodeBuffer, _s_d + 50, 2);                                                          //second+third byte of current Panel2
            // delayMicroseconds(250);                                                                           //give memset a little bit of time to empty all the buffers
            // _currentPanel2[_YC600_index] = _extractValue(0, 3, ((float)27.5 / (float)4096), 0, tempDecodeBuffer); //[A]
            //same as above in one line:
            _currentPanel2[_YC600_index] = (_extractValue(53, 1, (float)256, 0, _s_d) + _extractValue(50, 2, 1, 0, _s_d)) * (float)27.5 / (float)4096; //[A]

            // memset(&tempDecodeBuffer[0], 0, sizeof(tempDecodeBuffer));                                       //zero out all buffers we could work with "messageToDecode"
            // delayMicroseconds(250);                                                                          //give memset a little bit of time to empty all the buffers
            // strncpy(tempDecodeBuffer, _s_d + 48, 2);                                                         //first+second byte of voltage Panel1
            // delayMicroseconds(250);                                                                          //give memset a little bit of time to empty all the buffers
            // strncat(tempDecodeBuffer, _s_d + 46, 1);                                                         //third byte of voltage Panel1
            // delayMicroseconds(250);
            // #ifdef DEBUG_DECODE_MESSAGE_BUFFER
            //             DebugSwSer.print("**** DEBUG_DECODE_MESSAGE_BUFFER = ");
            //             DebugSwSer.print(_YC600_index);
            //             DebugSwSer.println(" Voltage: ");
            //             DebugSwSer.print("tempDecodeBuffer: ");
            //             DebugSwSer.print(tempDecodeBuffer);
            //             DebugSwSer.flush();
            // #endif                                                                         //give memset a little bit of time to empty all the buffers
            // _voltagePanel1[_YC600_index] = _extractValue(0, 3, ((float)82.5 / (float)4096), 0, tempDecodeBuffer); //[V]
            //same as above in one line:
            _voltagePanel1[_YC600_index] = (_extractValue(48, 2, (float)16, 0, _s_d) + _extractValue(46, 1, 1, 0, _s_d)) * (float)82.5 / (float)4096; //[V]

            // memset(&tempDecodeBuffer[0], 0, sizeof(tempDecodeBuffer));                                       //zero out all buffers we could work with "messageToDecode"
            // delayMicroseconds(250);                                                                          //give memset a little bit of time to empty all the buffers
            // strncpy(tempDecodeBuffer, _s_d + 54, 2);                                                         //first+second byte of voltage Panel2
            // delayMicroseconds(250);                                                                          //give memset a little bit of time to empty all the buffers
            // strncat(tempDecodeBuffer, _s_d + 52, 1);                                                         //third byte of voltage Panel2
            // delayMicroseconds(250);                                                                          //give memset a little bit of time to empty all the buffers
            // _voltagePanel2[_YC600_index] = _extractValue(0, 3, ((float)82.5 / (float)4096), 0, tempDecodeBuffer); //[V]
            //same as above in one line:
            _voltagePanel2[_YC600_index] = (_extractValue(54, 2, (float)16, 0, _s_d) + _extractValue(52, 1, 1, 0, _s_d)) * (float)82.5 / (float)4096; //[V]

            _t1[_YC600_index] = _t2[_YC600_index];
            _t2[_YC600_index] = _extractValue(30, 8, 1, 0, _s_d);

            _e11[_YC600_index] = _e12[_YC600_index];
            _e12[_YC600_index] = _extractValue(84, 6, 1, 0, _s_d);
            _e10[_YC600_index] = (_e12[_YC600_index] * 8.311F / (float)3600 / (float)1000); //[kWh]

            _p1[_YC600_index] = ((_e12[_YC600_index] - _e11[_YC600_index]) * 8.311F / (_t2[_YC600_index] - _t1[_YC600_index])); //[W]

            _e21[_YC600_index] = _e22[_YC600_index];
            _e22[_YC600_index] = _extractValue(74, 6, 1, 0, _s_d);
            _e20[_YC600_index] = (_e22[_YC600_index] * 8.311F / (float)3600 / (float)1000); //[kWh]

            _p2[_YC600_index] = ((_e22[_YC600_index] - _e21[_YC600_index]) * 8.311F / (_t2[_YC600_index] - _t1[_YC600_index])); //[W]

#if (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT) || (defined DEBUG_RECEIVED_VALUES)
            DebugSwSer.print("**** _YC600_index = ");
            DebugSwSer.print(_YC600_index);
            DebugSwSer.println();
            DebugSwSer.print("Decode: frequency: ");
            DebugSwSer.printf("%.2f", _frequency[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: temperature: ");
            DebugSwSer.printf("%.2f", _temperature[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: AC voltage: ");
            DebugSwSer.printf("%.2f", _acVoltage[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: current Panel1: ");
            DebugSwSer.printf("%.2f", _currentPanel1[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: current Panel2: ");
            DebugSwSer.printf("%.2f", _currentPanel2[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: voltage Panel1: ");
            DebugSwSer.printf("%.2f", _voltagePanel1[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: voltage Panel2: ");
            DebugSwSer.printf("%.2f", _voltagePanel2[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: power Panel1: ");
            DebugSwSer.printf("%.2f", _p1[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: power Panel2: ");
            DebugSwSer.printf("%.2f", _p2[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: energy Panel1: ");
            DebugSwSer.printf("%.2f", _e10[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.print("Decode: energy Panel2: ");
            DebugSwSer.printf("%.2f", _e20[_YC600_index]);
            DebugSwSer.println();
            DebugSwSer.println();
            DebugSwSer.flush();
#endif

            if (_ready == true && _paired == true)
            {
                _actualState = 22; //we decoded a new message
#ifdef DEBUG_OPERATION
                DebugSwSer.print("**** Decoded information's from inverter number = ");
                DebugSwSer.print(_YC600_index);
                DebugSwSer.println();
                DebugSwSer.print("frequency: ");
                DebugSwSer.printf("%.2f", _frequency[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("temperature: ");
                DebugSwSer.printf("%.2f", _temperature[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("AC voltage: ");
                DebugSwSer.printf("%.2f", _acVoltage[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("current Panel1: ");
                DebugSwSer.printf("%.2f", _currentPanel1[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("current Panel2: ");
                DebugSwSer.printf("%.2f", _currentPanel2[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("voltage Panel1: ");
                DebugSwSer.printf("%.2f", _voltagePanel1[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("voltage Panel2: ");
                DebugSwSer.printf("%.2f", _voltagePanel2[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("power Panel1: ");
                DebugSwSer.printf("%.2f", _p1[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("power Panel2: ");
                DebugSwSer.printf("%.2f", _p2[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("energy Panel1: ");
                DebugSwSer.printf("%.2f", _e10[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.print("energy Panel2: ");
                DebugSwSer.printf("%.2f", _e20[_YC600_index]);
                DebugSwSer.println();
                DebugSwSer.println();
                DebugSwSer.flush();
                delayMicroseconds(250);
#endif
            }
        }
    }
    else
    {
        Message_begin_offset = 12;

        memset(&_s_d[0], 0, sizeof(_s_d)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);            //give memset a little bit of time to empty all the buffers
        strncpy(_s_d, messageToDecode + 4 + Message_begin_offset, strlen(messageToDecode) + 1);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        memset(&_s_l_string[0], 0, sizeof(_s_l_string)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                          //give memset a little bit of time to empty all the buffers
        // strncpy(_s_l_string, _s_d+38, 12); //we use _s_l_string in this case as extrac buffer for ECU-ID-reverse
        // delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        memset(&_i_i[0], 0, sizeof(_i_i)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);            //give memset a little bit of time to empty all the buffers
        strncpy(_i_i, _s_d + 104, 12);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        // in lua:  if string.sub(str,5,8) == "4481" and string.sub(str,17,20) ~= "0000" then
        // in lua:    trace("Found inverter: 0x"..string.sub(str,19,20)..string.sub(str,17,18))
        // in lua:  end
        // Expected Answer which we would receive if the Inverter short ID is found
        memset(&tempMsgBuffer[0], 0, sizeof(tempMsgBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                              //give memset a little bit of time to empty all the buffers
        strncpy(tempMsgBuffer, _s_d + 62, 8);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_DECODE_MESSAGE
        DebugSwSer.println();
        DebugSwSer.print("Decode: _i_i: ");
        DebugSwSer.println(_i_i);
        DebugSwSer.print("Decode: _tempSearchInverterLongIdBuffer: ");
        DebugSwSer.println(_tempSearchInverterLongIdBuffer);
        DebugSwSer.print("Decode: strcmp(_i_i, _tempSearchInverterLongIdBuffer): ");
        DebugSwSer.println(strcmp(_i_i, _tempSearchInverterLongIdBuffer));
        DebugSwSer.print("Decode: strcmp(tempMsgBuffer, _CC2530_answer_string): ");
        DebugSwSer.println(strcmp(tempMsgBuffer, _CC2530_answer_string));
        DebugSwSer.print("Decode: _s_d: ");
        DebugSwSer.println(_s_d);
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        if (strcmp(tempMsgBuffer, _CC2530_answer_string) == 0 && strcmp(_i_i, _tempSearchInverterLongIdBuffer) == 0)
        {
            memset(&tempMsgBuffer[0], 0, sizeof(tempMsgBuffer)); //zero out all buffers we could work with "messageToDecode"
            delayMicroseconds(250);                              //give memset a little bit of time to empty all the buffers
            strncpy(tempMsgBuffer, _s_d + 76, 2);
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
            strncat(tempMsgBuffer, _s_d + 74, 2);
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

            if ((strcmp(tempMsgBuffer, "0000") != 0) && (strcmp(tempMsgBuffer, "FFFF") != 0) /*&& _i_i == InvSns*/)
            {
                memset(&_tempInverterShortIdBuffer[0], 0, sizeof(_tempInverterShortIdBuffer)); //zero out all buffers we could work with "messageToDecode"
                delayMicroseconds(250);                                                        //give memset a little bit of time to empty all the buffers
                strncpy(_tempInverterShortIdBuffer, tempMsgBuffer, 4);                         //copy the found short ID into an temporary Buffer
                delayMicroseconds(250);                                                        //give memset a little bit of time to empty all the buffers

                memset(&_tempFoundInverterLongIdBuffer[0], 0, sizeof(_tempFoundInverterLongIdBuffer)); //zero out all buffers we could work with "messageToDecode"
                delayMicroseconds(250);                                                                //give memset a little bit of time to empty all the buffers
                strncpy(_tempFoundInverterLongIdBuffer, _i_i, 12);                                     //copy the found short ID into an temporary Buffer
                delayMicroseconds(250);                                                                //give memset a little bit of time to empty all the buffers
                _got_inverter_short_number = true;
                _actualState = 34; //we found an inverter short ID by auto pair routine
#ifdef DEBUG_OPERATION
                DebugSwSer.print("Pairing found Inverter short ID: ");
                DebugSwSer.print(_tempInverterShortIdBuffer);
                DebugSwSer.print(" with serial number: ");
                DebugSwSer.println(_tempFoundInverterLongIdBuffer);
                DebugSwSer.flush();
                delayMicroseconds(250);
#endif
            }
            else
            {
                _actualState = -1; //we didn't found an inverter short ID by auto pair routine
#ifdef DEBUG_OPERATION
                DebugSwSer.print("Pairing inverter doesn't respond with the right short ID: ");
                DebugSwSer.print(tempMsgBuffer);
                DebugSwSer.print(" from inverter with serial number: ");
                DebugSwSer.print(_tempFoundInverterLongIdBuffer);
                DebugSwSer.println(" trying again! ");
                DebugSwSer.flush();
                delayMicroseconds(250);
#endif
                _got_inverter_short_number = false;
                _loopcounterPairing = 0; // we should start at command 1 (array 0) again after we successfully paired 1 inverter
            }

// here we need to safe Inv_Sns[i] and tempMsgBuffer into the EEPROM for each inverter individually
#ifdef DEBUG_DECODE_MESSAGE
            DebugSwSer.print("Decode: found inverter: ");
            DebugSwSer.print(tempMsgBuffer);
            DebugSwSer.println();
            DebugSwSer.println();
            DebugSwSer.flush();
#endif
        }
    }

    if (strcmp(messageToDecode, _resetString) == 0) // default answer to reset we compare the whole message
    {
#if (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT) || (defined DEBUG_RECEIVED_VALUES)
        DebugSwSer.print("Decode: reset detectet");
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif
        _actualState = 10; //reset of CC2530 detected
#ifdef DEBUG_OPERATION
        DebugSwSer.println("Reset detected!");
        DebugSwSer.flush();
        delayMicroseconds(250);
#endif
    }

    if (strcmp(messageToDecode, _noAnswerFromInverter) == 0) // default answer if the asked inverter doesn't send us values we compare the whole message
    {
#if (defined DEBUG_DECODE_MESSAGE_DATA_EXTRACT) || (defined DEBUG_RECEIVED_VALUES)
        DebugSwSer.print("Decode: no answer from Inverter!!");
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif
#ifdef DEBUG_OPERATION
        DebugSwSer.println("No answer from Inverter!");
        DebugSwSer.flush();
        delayMicroseconds(250);
#endif
    }

#ifdef DEBUG_DECODE_MESSAGE
    DebugSwSer.print("Decode: ");
    DebugSwSer.println();
    DebugSwSer.print("_resetString: ");
    DebugSwSer.print(_resetString);
    DebugSwSer.println();
    DebugSwSer.print("tempMsgBuffer: ");
    DebugSwSer.print(tempMsgBuffer);
    DebugSwSer.println();
    DebugSwSer.print("messageToDecode: ");
    DebugSwSer.print(messageToDecode);
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.println();
    // DebugSwSer.println();
    // DebugSwSer.print("oneChar: ");
    // DebugSwSer.println(oneChar);
    // DebugSwSer.print("_fullIncomingMessage: ");
    // DebugSwSer.println(_fullIncomingMessage);
    DebugSwSer.flush();
#endif

    _ready_to_send = true;
}

uint16_t _readcounter = 0;
void YC600::_read()
{
    _ready_to_send = false;

    _error = SENSOR_ERROR_OK;

    _readcounter = 0;

    memset(&_fullIncomingMessage[0], 0, sizeof(_fullIncomingMessage)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                            //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_SERIAL_RECEIVE
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.print("Serial.available(): ");
    DebugSwSer.print(_serial_available());
    DebugSwSer.println();
    DebugSwSer.print("_waiting_for_response: ");
    DebugSwSer.print(_waiting_for_response);

    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    while (_serial_available())
    {
        if (_readcounter < CC2530_MAX_SERIAL_BUFFER_SIZE)
        {
            _processIncomingByte(_serial_read());
        }
        else
        {
            _serial_read();
        }

        if (_serial_available() == 0)
        {
            uint16_t iToUpper = 0; // has to be 16bit because the received message if the YC600 answers is longer than 255
            while (_fullIncomingMessage[iToUpper])
            {
                _fullIncomingMessage[iToUpper] = toupper(_fullIncomingMessage[iToUpper]);
                iToUpper++;
            }
            _waiting_for_response = false;
            _decodeMessage(_fullIncomingMessage, _InvSns);
        }
    }
    if (_serial_available() == 0)
    {
        _waiting_for_response = false;
        _ready_to_send = true;
    }
}

uint8_t loopcounter = 0; //Don't know why static inside of the function wouldn't work
uint8_t localPaired = 0; //Don't know why static inside of the function wouldn't work
void YC600::_initializeCC2530(bool Paired)
{

    _error = SENSOR_ERROR_OK;

    // static uint8_t loopcounter = 0;  //Don't know why static inside of the function wouldn't work
    // static uint8_t localPaired = 0;  //Don't know why static inside of the function wouldn't work

#ifdef DEBUG_INITIALIZE_CMD
    DebugSwSer.println();
    DebugSwSer.println("++++ DEBUG_INITIALIZE_CMD");
    DebugSwSer.print("loopcounter: ");
    DebugSwSer.println(loopcounter);
    DebugSwSer.print("localPaired: ");
    DebugSwSer.println(localPaired);
    DebugSwSer.flush();
#endif

    if (Paired == true)
    {
        localPaired = 0;
    }
    else
    {
        localPaired = 1;
    }

#ifdef DEBUG_INITIALIZE_CMD
    DebugSwSer.println();
    DebugSwSer.println("++++ DEBUG_INITIALIZE_CMD");
    DebugSwSer.print("loopcounter: ");
    DebugSwSer.println(loopcounter);
    DebugSwSer.print("_reset_done: ");
    DebugSwSer.println(_reset_done);
    DebugSwSer.print("_ready: ");
    DebugSwSer.println(_ready);
    DebugSwSer.print("localPaired: ");
    DebugSwSer.println(localPaired);
    DebugSwSer.print("_serial_available(): ");
    DebugSwSer.println(_serial_available());
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    if (loopcounter == 0)
    {
        _serial_read(); //only send if the serial buffer is empty
    }
    if (!_serial_available())
    {

#ifdef DEBUG_INITIALIZE_CMD
        DebugSwSer.println();
        DebugSwSer.println("++++ DEBUG_INITIALIZE_CMD second:");
        DebugSwSer.print("loopcounter: ");
        DebugSwSer.println(loopcounter);
        DebugSwSer.print("_reset_done: ");
        DebugSwSer.println(_reset_done);
        DebugSwSer.print("_ready: ");
        DebugSwSer.println(_ready);
        DebugSwSer.print("localPaired: ");
        DebugSwSer.println(localPaired);
        DebugSwSer.print("_serial_available(): ");
        DebugSwSer.println(_serial_available());
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        if (loopcounter < CC2530_STARTUP_RESET_TIMES && _reset_done == false) // send 5 times reset command
        {

#ifdef DEBUG_INITIALIZE_CMD
            DebugSwSer.println();
            DebugSwSer.println("++++ DEBUG_INITIALIZE_CMD second:");
            DebugSwSer.print("loopcounter: ");
            DebugSwSer.println(loopcounter);
            DebugSwSer.print("CC2530_STARTUP_RESET_TIMES: ");
            DebugSwSer.println(CC2530_STARTUP_RESET_TIMES);
            DebugSwSer.print("_reset_done: ");
            DebugSwSer.println(_reset_done);
            DebugSwSer.print("localPaired: ");
            DebugSwSer.println(localPaired);
            DebugSwSer.print("_serial_available(): ");
            DebugSwSer.println(_serial_available());
            DebugSwSer.println();
            DebugSwSer.flush();
#endif

            _serial_print(CodedCommands[1]); //TODO use _calculateNumberOfCommands function
        }

        if (loopcounter == CC2530_STARTUP_RESET_TIMES && _reset_done == false)
        {
            loopcounter = 0;
            _reset_done = true;
        }

        if (loopcounter < _calculateNumberOfCommands() - 1 - localPaired && _reset_done == true && _ready == false) //send all commands one after the other in the right order
        {
#ifdef DEBUG_INITIALIZE_CMD
            DebugSwSer.println();
            DebugSwSer.print("_calculateNumberOfCommands(): ");
            DebugSwSer.println(_calculateNumberOfCommands());
            DebugSwSer.println();
            DebugSwSer.flush();
#endif

            _serial_print(CodedCommands[loopcounter]); //TODO use _calculateNumberOfCommands function
            if (loopcounter == _calculateNumberOfCommands() - 2 - localPaired)
            {
                _ready = true;
            }
        }
        if (loopcounter < 50) //just to make sure it never overruns
        {
            loopcounter++;
        }
    }
}

uint8_t operatecounter = 0; // I don't know why static inside of the function didn't work in C++
void YC600::_operate(char EcuId[], bool Paired, char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20])
{
    //static uint8_t operatecounter = 0; // I don't know why static inside of the function didn't work in C++

    if (operatecounter >= _numberOfInverters) //-1 because we start the array at 0
    {
        operatecounter = 0;
    }

    _serial_print(normalOperationInverterCommands[operatecounter]);

#ifdef DEBUG_NORMAL_OPERATION_MODE
    DebugSwSer.println();
    DebugSwSer.print("*** operatecounter: ");
    DebugSwSer.println(operatecounter);
    DebugSwSer.print("calculateNumberOfInverters(*InvSns): ");
    DebugSwSer.println(calculateNumberOfInverters(*InvSns));
    DebugSwSer.print("calculateNumberOfInverters(*InvSns)-1: ");
    DebugSwSer.println(calculateNumberOfInverters(*InvSns) - 1);
    //                      DebugSwSer.print("_ready: ");
    //                      DebugSwSer.println(_ready);
    //                      DebugSwSer.print("localPaired: ");
    //                      DebugSwSer.println(localPaired);
    //                      DebugSwSer.println();
    DebugSwSer.flush();
#endif

    operatecounter++;
}

// ---------------------------------------------------------------------

bool YC600::_serial_is_hardware()
{
    return (3 == _pin_rx) || (13 == _pin_rx);
}

bool YC600::_serial_available()
{
    if (_serial_is_hardware())
    {
        return Serial.available();
    }
    else
    {
        return _serial->available();
    }
}

void YC600::_serial_flush()
{
    if (_serial_is_hardware())
    {
        return Serial.flush();
    }
    else
    {
        return _serial->flush();
    }
}

uint8_t YC600::_serial_read()
{
    if (_serial_is_hardware())
    {
        return Serial.read();
    }
    else
    {
        return _serial->read();
    }
}

int YC600::_serial_availableForWrite()
{
    if (_serial_is_hardware())
    {
        return Serial.availableForWrite();
    }
    else
    {
        return _serial->availableForWrite();
    }
}

void YC600::_serial_write(int transmit)
{
    if (_serial_is_hardware())
    {
        Serial.write(transmit);
    }
    else
    {
        _serial->write(transmit);
    }
}

// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint char* concat(const char *s1, const char *s2)
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint {
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint     char *result = (char*)malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint     // in real code you would check for errors in malloc here
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint     strcpy(result, s1);
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint     strcat(result, s2);
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint     return result;
// perhaps we could use this for sln and crc perhaps this is faster or has a smaller footprint }

char bufferSend[254];
void YC600::_serial_print(char printString[])
{

#ifdef DEBUG_SERIAL_PRINT
    DebugSwSer.print("DEBUG - _serial_print");
    DebugSwSer.println();
    DebugSwSer.print("_serial_availableForWrite(): ");
    DebugSwSer.println(_serial_availableForWrite());
    DebugSwSer.print("(uint8_t)strlen(printString): ");
    DebugSwSer.println((uint8_t)strlen(printString));
    DebugSwSer.flush();
#endif

    if (_serial_availableForWrite() > (uint8_t)strlen(printString))
    {
        _serial_write(0xFE); //we have to send "FE" for each command
        for (uint8_t i = 0; i <= strlen(printString) / 2 - 1; i++)
        {
            strncpy(bufferSend, printString + i * 2, 2); //divide each command into separate two chars to calculate bytes from this
            delayMicroseconds(250);                      //give memset a little bit of time to empty all the buffers
            _serial_write(_StrToHex(bufferSend));        //turn separate two chars to bytes and send them
        }
        _serial_flush(); //wait till the full command was sent

        // not good as it comes too fast after the other states so that only this one was visible
        // _actualState = 16; //we send a new string to the CC2530

#if (defined DEBUG_SERIAL_SENT) || (defined DEBUG_OPERATION)
        DebugSwSer.print("Sent: FE");
        DebugSwSer.println(printString);
        DebugSwSer.flush();
        delayMicroseconds(250);
#endif

        _waiting_for_response = true;
    }
}

//function reads the global variable ecu_id and writes to global _ecu_id_reverse
void YC600::_getReverseId(char EcuId[])
{
#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** EcuId = %s", EcuId);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    if (_ecu_id_reverse[0] == 0)
    {
        strncpy(_ecu_id_reverse, EcuId + 10, 2); //Vorher strncat, hoffentlich funktioniert es noch
        delayMicroseconds(250);                  //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_reverse, EcuId + 8, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_reverse, EcuId + 6, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_reverse, EcuId + 4, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_reverse, EcuId + 2, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_reverse, EcuId, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    }

#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** _ecu_id_reverse = %s", _ecu_id_reverse);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
}

//function reads the global variable ecu_id and writes to global _ecu_id_short
void YC600::_getShortId(char EcuId[])
{
#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** EcuId = %s", EcuId);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    if (_ecu_id_short[0] == 0)
    {
        strncpy(_ecu_id_short, EcuId, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_ecu_id_short, EcuId + 2, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    }

#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** _ecu_id_short = %s", _ecu_id_short);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
}

//function reads the global variable ecu_id and writes to global _ecu_id_short
void YC600::_getReverseShortId(char EcuId[])
{
#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** EcuId = %s", EcuId);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    if (_reverse_ecu_id_short[0] == 0)
    {
        strncpy(_reverse_ecu_id_short, EcuId + 2, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(_reverse_ecu_id_short, EcuId, 2);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    }

#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** _ecu_id_short = %s", _ecu_id_short);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
}

//function uses the global array CodedCommands and calculates the number of the commands
uint8_t YC600::_calculateNumberOfCommands(void)
{
    uint8_t numberOfCommands = (uint8_t)(sizeof(CodedCommands) / sizeof(CodedCommands[0]));
#ifdef DEBUG_BUILD_ID
    DebugSwSer.printf("**** number of commands = %u", numberOfCommands);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    return numberOfCommands;
}

//function uses the global array CodedCommands and calculates the number of the commands
uint8_t YC600::_calculateNumberOfPairingCommands(void)
{
    uint8_t numberOfPairingCommands = (uint8_t)(sizeof(pairingOperationInverterCommands) / sizeof(pairingOperationInverterCommands[0]));
#ifdef DEBUG_CALCULATE_NUMBER_PAIRING_COMMANDS
    DebugSwSer.printf("**** numberOfPairingCommands = %u", numberOfPairingCommands);
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
    return numberOfPairingCommands;
}

void YC600::_buildCommand3(char EcuId[])
{
    _getReverseId(EcuId);

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** _ecu_id_reverse = %s", _ecu_id_reverse);
    DebugSwSer.println();
    DebugSwSer.printf("**** before CodedCommands[2] = %s", CodedCommands[2]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    strncat(CodedCommands[2], _ecu_id_reverse, sizeof(_ecu_id_reverse)); //TODO use _calculateNumberOfCommands function
    delayMicroseconds(250);                                              //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** after CodedCommands[2] = %s", CodedCommands[2]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
}

void YC600::_buildCommand5(char EcuId[])
{
    _getShortId(EcuId);

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** _ecu_id_short = %s", _ecu_id_short);
    DebugSwSer.println();
    DebugSwSer.printf("**** before CodedCommands[4] = %s", CodedCommands[4]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    strncat(CodedCommands[4], _ecu_id_short, sizeof(_ecu_id_short)); //TODO use _calculateNumberOfCommands function

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** after CodedCommands[4] = %s", CodedCommands[4]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif
}

void YC600::_buildCommand10(char EcuId[])
{
    _getReverseId(EcuId);

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** _ecu_id_reverse = %s", _ecu_id_reverse);
    DebugSwSer.println();
    DebugSwSer.printf("**** before CodedCommands[9] = %s", CodedCommands[9]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    strncat(CodedCommands[9], _ecu_id_reverse, sizeof(_ecu_id_reverse));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** middle CodedCommands[9] = %s", CodedCommands[9]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    strncat(CodedCommands[9], CodedCommands[10], sizeof(CodedCommands[10])); //TODO use _calculateNumberOfCommands function
    delayMicroseconds(250);                                                  //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("**** after CodedCommands[9] = %s", CodedCommands[9]); //TODO use _calculateNumberOfCommands function
    DebugSwSer.println();
    DebugSwSer.flush();
#endif

    //CodedCommands[10] = ""; //TODO use _calculateNumberOfCommands function //delete the last command because it is not used anymore
    strcpy(CodedCommands[10], ""); //TODO use _calculateNumberOfCommands function //delete the last command because it is not used anymore
    delayMicroseconds(250);        //give memset a little bit of time to empty all the buffers
}

void YC600::_buildNormalOperationCommands(char EcuId[], char InvIds[YC600_MAX_NUMBER_OF_INVERTERS][8]) // I have no clue why I have to use more than 5 (6 is not enough too) just for the strncpy to do its work //sending of messages fail otherwise
{
    _getReverseId(EcuId);

    memset(&normalOperationInverterCommands[0], 0, sizeof(normalOperationInverterCommands)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                                                  //give memset a little bit of time to empty all the buffers

    if (normalOperationInverterCommands[0][0] == 0) //only run if normalOperationInverterCommands arent't initialized before
    {
#ifdef DEBUG_BUILD_NORMAL_COMMANDS_CONCAT
        DebugSwSer.println();
        DebugSwSer.print("+++ starting  normalOperationInverterCommands[0][0] = ");
        DebugSwSer.println(normalOperationInverterCommands[0][0]);
        DebugSwSer.print("InvIds[0]: ");
        DebugSwSer.println(InvIds[0]);
        DebugSwSer.print("InvIds[1]: ");
        DebugSwSer.println(InvIds[1]);
        DebugSwSer.print("_InvIds[0]: ");
        DebugSwSer.println(_InvIds[0]);
        DebugSwSer.print("_InvIds[1]: ");
        DebugSwSer.println(_InvIds[1]);
        //DebugSwSer.println(InvIds[2]);
        //DebugSwSer.print("  _InvIds[i] = ");
        //DebugSwSer.print(_InvIds[i]);
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        for (uint8_t i = 0; i <= _numberOfInverters - 1; i++) //-1 because we start the array at 0
        {
            strncpy(normalOperationInverterCommands[i], normalOperationBaseCommand[0], strlen(normalOperationBaseCommand[0]));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
            strncat(normalOperationInverterCommands[i], InvIds[i] + 2, 2);
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
            strncat(normalOperationInverterCommands[i], InvIds[i], 2);
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
            strncat(normalOperationInverterCommands[i], normalOperationBaseCommand[1], sizeof(normalOperationBaseCommand[1]));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

            strncat(normalOperationInverterCommands[i], _ecu_id_reverse, sizeof(_ecu_id_reverse));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

            strncat(normalOperationInverterCommands[i], normalOperationBaseCommand[2], sizeof(normalOperationBaseCommand[2]));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

            //see initilization Command buildup for details
            strcpy(normalOperationInverterCommands[i], strncat(_sln(normalOperationInverterCommands[i]), normalOperationInverterCommands[i], sizeof(_sln(normalOperationInverterCommands[i])) + sizeof(normalOperationInverterCommands[i]))); //build command plus sln at the beginning
            delayMicroseconds(250);                                                                                                                                                                                                           //give memset a little bit of time to empty all the buffers

            // put in the CRC at the end of the command
            // we make this in the initialisation function because these commands didn't change after all //same short-way as above
            strcpy(normalOperationInverterCommands[i], strncat(normalOperationInverterCommands[i], _crc(normalOperationInverterCommands[i]), sizeof(normalOperationInverterCommands[i]) + sizeof(_crc(normalOperationInverterCommands[i]))));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_BUILD_NORMAL_COMMANDS_CONCAT
            DebugSwSer.println();
            DebugSwSer.print("calculateNumberOfInverters(InvIds): ");
            DebugSwSer.println(calculateNumberOfInverters(*InvIds));
            DebugSwSer.println();
            DebugSwSer.print("_ecu_id_reverse: ");
            DebugSwSer.println(_ecu_id_reverse);
            DebugSwSer.println();

            DebugSwSer.println();
            DebugSwSer.print("*** i = ");
            DebugSwSer.print(i);
            DebugSwSer.print("  normalOperationInverterCommands[i] = ");
            DebugSwSer.print(normalOperationInverterCommands[i]);
            //DebugSwSer.println(InvIds[0]);
            DebugSwSer.print("InvIds[i]: ");
            DebugSwSer.println(InvIds[i]);
            DebugSwSer.print("  _InvIds[i] = ");
            DebugSwSer.println(_InvIds[i]);
            DebugSwSer.println();
            DebugSwSer.flush();
#endif
        }
    }
}

// char PairingBaseCommand[][254] =
// {
//     "6700",
//     "24020FFFFFFFFFFFFFFFFF14FFFF140D0200000F1100"..inv_sn.."FFFF10FFFF"..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2),
//     "24020FFFFFFFFFFFFFFFFF14FFFF140C0201000F0600"..inv_sn,
//     "24020FFFFFFFFFFFFFFFFF14FFFF140F0102000F1100"..inv_sn..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2).."10FFFF"..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2),
//     "24020FFFFFFFFFFFFFFFFF14FFFF14010103000F0600"..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2)
// };

char PairingBaseCommand[][254] =
    {
        //Command 1:
        "6700",
        //Command 2:
        "24020FFFFFFFFFFFFFFFFF14FFFF140D0200000F1100",
        //..inv_sn..
        "FFFF10FFFF",
        //..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2),
        //Command 3:
        "24020FFFFFFFFFFFFFFFFF14FFFF140C0201000F0600",
        //..inv_sn,
        //Command 4:
        "24020FFFFFFFFFFFFFFFFF14FFFF140F0102000F1100",
        //..inv_sn..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2)..
        "10FFFF",
        //..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2),
        //Command 5:
        "24020FFFFFFFFFFFFFFFFF14FFFF14010103000F0600",
        //..string.sub(ecu_id,11,12)..string.sub(ecu_id,9,10)..string.sub(ecu_id,7,8)..string.sub(ecu_id,5,6)..string.sub(ecu_id,3,4)..string.sub(ecu_id,1,2)
};

//now it gets crazy 3 Dimensional
//here I try to build together all the commands to pair up to 5 inverters indipendently
void YC600::_buildPairingCommands(char EcuId[], char InvSerial[YC600_MAX_NUMBER_OF_INVERTERS][20]) // I have no clue why I have to use more than 5 (6 is not enough too) just for the strncpy to do its work //sending of messages fail otherwise
{
    _getReverseId(EcuId);
    _getShortId(EcuId);
    _getReverseShortId(EcuId);
    //loop through number of inverter dimension
    for (uint8_t _inverterNumber = 0; _inverterNumber <= _numberOfInverters - 1; _inverterNumber++)
    {
        //build command1 (index[_inverterNumber][0])
        strncpy(pairingOperationInverterCommands[_inverterNumber][0], PairingBaseCommand[0], strlen(PairingBaseCommand[0]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        //build command2 (index[_inverterNumber][1])
        strncpy(pairingOperationInverterCommands[_inverterNumber][1], PairingBaseCommand[1], strlen(PairingBaseCommand[1]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][1], InvSerial[_inverterNumber], strlen(InvSerial[_inverterNumber]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][1], PairingBaseCommand[2], strlen(PairingBaseCommand[2]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][1], _ecu_id_reverse, sizeof(_ecu_id_reverse));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        //build command3 (index[_inverterNumber][2])
        strncpy(pairingOperationInverterCommands[_inverterNumber][2], PairingBaseCommand[3], strlen(PairingBaseCommand[3]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][2], InvSerial[_inverterNumber], strlen(InvSerial[_inverterNumber]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        //build command4 (index[_inverterNumber][3])
        strncpy(pairingOperationInverterCommands[_inverterNumber][3], PairingBaseCommand[4], strlen(PairingBaseCommand[4]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][3], InvSerial[_inverterNumber], strlen(InvSerial[_inverterNumber]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][3], _reverse_ecu_id_short, sizeof(_reverse_ecu_id_short));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][3], PairingBaseCommand[5], strlen(PairingBaseCommand[5]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][3], _ecu_id_reverse, sizeof(_ecu_id_reverse));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        //build command5 (index[_inverterNumber][4])
        strncpy(pairingOperationInverterCommands[_inverterNumber][4], PairingBaseCommand[6], strlen(PairingBaseCommand[6]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(pairingOperationInverterCommands[_inverterNumber][4], _ecu_id_reverse, sizeof(_ecu_id_reverse));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        //loop through number of commands to build together all individual paring commands

        for (uint8_t _command = 0; _command <= MAX_PAIRING_COMMANDS - 1; _command++)
        {

            // in short this should be possible too:
            strcpy(pairingOperationInverterCommands[_inverterNumber][_command], strncat(_sln(pairingOperationInverterCommands[_inverterNumber][_command]), pairingOperationInverterCommands[_inverterNumber][_command], sizeof(_sln(pairingOperationInverterCommands[_inverterNumber][_command])) + sizeof(pairingOperationInverterCommands[_inverterNumber][_command]))); //build command plus sln at the beginning
            delayMicroseconds(250);                                                                                                                                                                                                                                                                                                                                        //give memset a little bit of time to empty all the buffers

            // put in the CRC at the end of the command
            // we make this in the initialisation function because these commands didn't change after all //same short-way as above
            strcpy(pairingOperationInverterCommands[_inverterNumber][_command], strncat(pairingOperationInverterCommands[_inverterNumber][_command], _crc(pairingOperationInverterCommands[_inverterNumber][_command]), sizeof(pairingOperationInverterCommands[_inverterNumber][_command]) + sizeof(_crc(pairingOperationInverterCommands[_inverterNumber][_command]))));
            delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_BUILD_PAIRING_COMMANDS
            DebugSwSer.println();
            DebugSwSer.println("DEBUG_BUILD_PAIRING_COMMANDS: ");
            DebugSwSer.print("InvSerial[_inverterNumber]: ");
            DebugSwSer.println(InvSerial[_inverterNumber]);
            DebugSwSer.print("_inverterNumber: ");
            DebugSwSer.println(_inverterNumber);
            DebugSwSer.print("_command: ");
            DebugSwSer.println(_command);
            DebugSwSer.print("pairingOperationInverterCommands[_inverterNumber][_command]");
            DebugSwSer.println(pairingOperationInverterCommands[_inverterNumber][_command]);
            DebugSwSer.println();
            DebugSwSer.flush();
#endif
        }
    }
}

// function in lua function sln(str)
// function in lua   local l=string.format("%02x",string.len(str)/2-2)
// function in lua   return string.upper(l)
// function in lua end
char bufferSln[254];
char *YC600::_sln(char Command[])
{
    sprintf(bufferSln, "%02x", (strlen(Command) / 2 - 2));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

    uint8_t iToUpper = 0;
    while (bufferSln[iToUpper])
    {
        bufferSln[iToUpper] = toupper(bufferSln[iToUpper]);
        iToUpper++;
    }

#ifdef DEBUG_CONCAT
    DebugSwSer.printf("%s", bufferSln);
    DebugSwSer.flush();
#endif

    return bufferSln;
}

//function gets a string and returns a Hex value
int YC600::_StrToHex(char str[])
{
    return (int)strtol(str, 0, 16);
}

// int YC600::_HexToStr(char str[])
// {
//   return (int) ltostr(str, 0, 16);
// }

// function gets a string and returns a unsigned Hex value
// uint YC600::_StrTouHex(char str[])
// {
//   return (uint) strtoul(str, 0, 16);
// }

char bufferCRC[254] = {0};
char bufferCRCdiezweite[254] = {0};
int command_i;
uint command_ui;
int buffer_i;
uint calculatedCrc;

// function in lua function crc(str)
// function in lua   str=sln(str)..str
// function in lua   local crc=string.sub(str,0,2)
// function in lua   for i=1, string.len(str)/2-1 do
// function in lua     crc=string.format("%02x",bit.bxor(tonumber(crc,16),tonumber(string.sub(str,i*2+1,i*2+2),16)))
// function in lua   end
// function in lua   return string.upper(crc)
// function in lua end
// here we build the first line "str=sln(str)..str" outide of this function
char *YC600::_crc(char Command[])
{
    memset(&bufferCRC[0], 0, sizeof(bufferCRC));                   // zero out all buffers
    delayMicroseconds(250);                                        //give memset a little bit of time to empty all the buffers
    memset(&bufferCRCdiezweite[0], 0, sizeof(bufferCRCdiezweite)); // zero out all buffers
    delayMicroseconds(250);                                        //give memset a little bit of time to empty all the buffers

    strncpy(bufferCRC, Command, 2); //as starting point perhaps called "seed" use the first two chars from "Command"
    delayMicroseconds(250);         //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_CRC
    DebugSwSer.printf("**** Command = %s", Command);
    DebugSwSer.printf(" - crc=string.sub = %s => ", bufferCRC);
    DebugSwSer.flush();
#endif

    //  for (uint8_t i=1; i<=(strlen(Command)/2-1); i++)
    //  {
    //    strncpy(bufferCRCdiezweite, Command+i*2, 2 ); //use every iteration the next two chars starting with char 2+3
    //
    //    #ifdef DEBUG_CRC
    //    DebugSwSer.println();
    //    DebugSwSer.printf("**** i = %u", i);
    //    DebugSwSer.printf(" -firstXor= %x", _StrToHex(bufferCRC));
    //    DebugSwSer.printf(" -toNumberString= %s", bufferCRCdiezweite);
    //    DebugSwSer.printf(" -secondXor= %x", _StrToHex(bufferCRCdiezweite));
    //    #endif
    //
    //
    //    calculatedCrc = _StrToHex(bufferCRC)^_StrToHex(bufferCRCdiezweite); //calculate the bitwise XOR
    //
    //    #ifdef DEBUG_CRC
    //    DebugSwSer.printf(" -XOR= %u", calculatedCrc);
    //    #endif
    //
    //
    //    sprintf (bufferCRC, "%02x", calculatedCrc); //use the hexadecimal first two chars
    //
    //    #ifdef DEBUG_CRC
    //    DebugSwSer.printf(" -crc= %s", bufferCRC);
    //    #endif
    //  }
    // these version uses: RAM:   [===       ]  35.0% (used 28656 bytes from 81920 bytes)
    // these version uses: Flash: [===       ]  25.3% (used 264616 bytes from 1044464 bytes) //with a little bit of debug output but the same in all comparisons

    //  for (uint8_t i=1; i<=(strlen(Command)/2-1); i++)
    //  {
    //    strncpy(bufferCRCdiezweite, Command+i*2, 2 ); //use every iteration the next two chars starting with char 2+3
    //
    //    sprintf (bufferCRC, "%02x", _StrToHex(bufferCRC)^_StrToHex(bufferCRCdiezweite)); //use the hexadecimal first two chars //atoi instead of StrToHex at this point uses 16bytes more Flash StrTouHex uses 368bytes more Flash
    //
    //    #ifdef DEBUG_CRC
    //    DebugSwSer.printf(" -crc= %s", bufferCRC);
    //    #endif
    //  }
    // these version uses: RAM:   [===       ]  35.0% (used 28648 bytes from 81920 bytes)
    // these version uses: Flash: [===       ]  25.3% (used 264616 bytes from 1044464 bytes) //with a little bit of debug output but the same in all comparisons

    for (uint8_t i = 1; i <= (strlen(Command) / 2 - 1); i++)
    {
        strncpy(bufferCRCdiezweite, Command + i * 2, 2); //use every iteration the next two chars starting with char 2+3
        delayMicroseconds(250);                          //give memset a little bit of time to empty all the buffers

        sprintf(bufferCRC, "%02x", _StrToHex(bufferCRC) ^ _StrToHex(bufferCRCdiezweite));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_CRC
        DebugSwSer.printf(" -crc= %s", bufferCRC);
        DebugSwSer.flush();
#endif
    }

    uint8_t iToUpper = 0;
    while (bufferCRC[iToUpper])
    {
        bufferCRC[iToUpper] = toupper(bufferCRC[iToUpper]);
        iToUpper++;
    }

#ifdef DEBUG_CRC
    DebugSwSer.printf(" -crc= %s", bufferCRC);
    DebugSwSer.flush();
#endif

    return bufferCRC;
}

void YC600::_buildInitializeCommands(char EcuId[])
{
#ifdef DEBUG_INITIALIZE_CMD
    DebugSwSer.println("Commands before the magic:");
    for (uint8_t i = 0; i < _calculateNumberOfCommands(); i++) //-1 because the last element is used to concatenate the
    {
        //DebugSwSer.printf("**** i = %u", i);
        DebugSwSer.printf("%s", CodedCommands[i]);
        DebugSwSer.println();
        DebugSwSer.flush();
    }
#endif

    _buildCommand3(EcuId);
    _buildCommand5(EcuId);
    _buildCommand10(EcuId);

    for (uint8_t i = 0; i < _calculateNumberOfCommands(); i++) //loop through all commands and calculate sln plus crc
    {
        // char local_str_buffer[254];
        // strcpy(local_str_buffer,_sln(CodedCommands[i]));
        // strncat(local_str_buffer, CodedCommands[i], sizeof(local_str_buffer)+sizeof(CodedCommands[i]));
        // strcpy(CodedCommands[i], local_str_buffer);
        // leads to compiled RAM:   [===       ]  34.6% (used 28332 bytes from 81920 bytes)
        // leads to compiled Flash: [===       ]  25.3% (used 264404 bytes from 1044464 bytes)

        //char *beginning = "FE";

        // in short this should be possible too:
        strcpy(CodedCommands[i], strncat(_sln(CodedCommands[i]), CodedCommands[i], sizeof(_sln(CodedCommands[i])) + sizeof(CodedCommands[i]))); //build command plus sln at the beginning
        delayMicroseconds(250);                                                                                                                 //give memset a little bit of time to empty all the buffers
                                                                                                                                                //strcpy(CodedCommands[i], strncat(beginning, CodedCommands[i], sizeof(beginning)+sizeof(CodedCommands[i]))); //build command plus sln at the beginning
                                                                                                                                                // is the same but the way before ist more readable and the Flash/RAM usage is better here
                                                                                                                                                // leads to RAM:   [===       ]  34.6% (used 28332 bytes from 81920 bytes)
                                                                                                                                                // leads to Flash: [===       ]  25.3% (used 264372 bytes from 1044464 bytes)

        //optionally add "FE" to the beginning of the string because we have to send it in each command at the beginning
        //strcpy(CodedCommands[i], strncat("FE", CodedCommands[i], (sizeof("FE")+sizeof(CodedCommands[i])))); //build command plus sln at the beginning
        //sprintf(CodedCommands[i], "FE", CodedCommands[i]);
        //both versions are not working so I send it at every send() by hand

#ifdef DEBUG_BUILD_COMMANDS
        DebugSwSer.printf("**** i = %u => ", i);
        DebugSwSer.printf("FE");
        DebugSwSer.printf("%s", CodedCommands[i]);
        DebugSwSer.printf("%s", _crc(CodedCommands[i]));
        DebugSwSer.println();
        DebugSwSer.println();
        DebugSwSer.flush();
#endif

        // put in the CRC at the end of the command
        // we make this in the initialisation function because these commands didn't change after all //same short-way as above
        strcpy(CodedCommands[i], strncat(CodedCommands[i], _crc(CodedCommands[i]), sizeof(CodedCommands[i]) + sizeof(_crc(CodedCommands[i]))));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    }

#ifdef DEBUG_INITIALIZE_CMD
    DebugSwSer.println("Commands after the magic:");
    for (uint8_t i = 0; i < _calculateNumberOfCommands(); i++) //-1 because the last element is used to concatenate the
    {
        //DebugSwSer.printf("**** i = %u", i);
        DebugSwSer.printf("FE%s", CodedCommands[i]); //we have to send "FE" before every command so the "FE" is here on the right place but have to be there in every send(cmd) function
        DebugSwSer.println();
        DebugSwSer.flush();
    }
#endif
}

float YC600::_checkRange(float testValue, float minimumValue, float maximumValue)
{
    if (testValue < minimumValue)
    {
        return NAN;
    }
    else if (maximumValue < testValue)
    {
        return NAN;
    }
    else
    {
        return testValue;
    }
}

// Set 'pos' parameter to specify begin position of the string in memory
void YC600::_writeString(String str, int pos)
{
    unsigned int len = str.length();
    EEPROMr.write(pos, len); //write str-len at first position
    for (unsigned int i = 0; i < len; i++)
    {
        int s = str[i];
        EEPROMr.write(i + pos + DATA_OFFSET, s);
        delayMicroseconds(600);
    }
    EEPROMr.write(len + pos + DATA_OFFSET, '\0');
    delayMicroseconds(600);
    EEPROMr.commit();
    delayMicroseconds(600);
}

// Set 'pos' parameter to specify begin position of the string in memory
String YC600::_readString(int pos)
{

    int newStrLen = EEPROMr.read(pos);
    char data[newStrLen + 1];
    for (int i = 0; i < newStrLen; i++)
    {
        data[i] = char(EEPROMr.read(i + pos + DATA_OFFSET));
        delayMicroseconds(800);
    }
    data[newStrLen] = '\0'; // !!! NOTE !!! Remove the space between the slash "/" and "0" (I've added a space because otherwise there is a display bug)
    delayMicroseconds(800);

#ifdef DEBUG_EEPROM_READ_WRITE
    DebugSwSer.println("_readString(int pos) ");
    DebugSwSer.print("pos: ");
    DebugSwSer.println(pos);
    DebugSwSer.print("newStrLen: ");
    DebugSwSer.println(newStrLen);
    DebugSwSer.print("DATA_OFFSET: ");
    DebugSwSer.println(DATA_OFFSET);
    DebugSwSer.print("data: ");
    DebugSwSer.println(data);
#endif

    return String(data);
}

double YC600::_defaultCurrentRatio()
{
    return 1.0;
}

double YC600::_defaultVoltageRatio()
{
    return 1.0;
}

double YC600::_defaultPowerRatio()
{
    return 1.0;
}

double YC600::_defaultEnergyRatio()
{
    return 1.0;
}

//resets the EEPROM
void YC600::_resetPaired()
{
#ifdef DEBUG_RESET_PAIRED
    DebugSwSer.println("++++ _resetPaired!!!! ");
    DebugSwSer.flush();
#endif
    char TEMP_ecu_id[] = "AAAAAAAAAAAA";
    char TEMP_shortinv_id[] = "BBBB";
    char TEMP_longinv_id[] = "CCCCCCCCCCCC";
    for (uint8_t i = 0; i < YC600_MAX_NUMBER_OF_INVERTERS; i++)
    {
        _writePaired(i, TEMP_longinv_id, TEMP_shortinv_id, TEMP_ecu_id, _deleted);
    }
    _tryingToPairInverterNumber = 0; // if we detect that one inverter isn't paired we would like to pair all inverters agains and start with the first one index=0
#ifdef DEBUG_OPERATION
    DebugSwSer.println("++++ reset paired information's from EEPROM! ");
    DebugSwSer.flush();
    delayMicroseconds(250);
#endif
}
bool YC600::_writePaired(uint8_t inverterNumber, char InvSerial[14], char InverterShortId[6], char EcuId[], char Status[])
{

    //TODO Not working right now (for loop is not the right thing to use here) we have to look if the inverter short ID is already in the EEPROM or not and then calculate the "i" according to the inverter number, so that the array of the short ID matches the ID of the Sns
    //TODO safing of the inverter short ID to the EEPROM shoud work in this way but not the calculation of the right "i"

    //strncpy(EEPROM_inv_sns_in[inverterNumber], InvSerial[inverterNumber], strlen(InvSerial[inverterNumber]));
    //// Save string to flash memory (EEPROM library). Try to comment this line after the first run.
    //_writeString(EEPROM_inv_sns_in[inverterNumber], EEPROM_SAFE_ADRESS+40*inverterNumber);
    //
    //strncpy(EEPROM_inv_ids_in[inverterNumber], InvIds[inverterNumber], strlen(InvIds[inverterNumber]));
    //// Save string to flash memory (EEPROM library). Try to comment this line after the first run.
    //_writeString(EEPROM_inv_ids_in[inverterNumber], EEPROM_SAFE_ADRESS+(40*inverterNumber)+21);
    //
    //strncpy(EEPROM_buffer_paired_in[inverterNumber], "PAIRED", strlen("PAIRED"));
    //// Save string to flash memory (EEPROM library). Try to comment this line after the first run.
    //_writeString(EEPROM_buffer_paired_in[inverterNumber], EEPROM_SAFE_ADRESS+(40*inverterNumber)+21+30);

    memset(&EEPROM_writeBufferString[0], 0, sizeof(EEPROM_writeBufferString)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                                    //give memset a little bit of time to empty all the buffers
    strncpy(EEPROM_writeBufferString[inverterNumber], InvSerial, strlen(InvSerial));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(EEPROM_writeBufferString[inverterNumber], InverterShortId, strlen(InverterShortId));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(EEPROM_writeBufferString[inverterNumber], Status, strlen(Status));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(EEPROM_writeBufferString[inverterNumber], EcuId, strlen(EcuId));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    _writeString(EEPROM_writeBufferString[inverterNumber], EEPROM_SAFE_ADRESS + (40 * inverterNumber));
    delayMicroseconds(500); //give memset a little bit of time to empty all the buffers

    strncpy(_InvIds[inverterNumber], InverterShortId, strlen(InverterShortId)); //in this case we don't have the right InvSns in the EEPROM
    delayMicroseconds(250);                                                     //give memset a little bit of time to empty all the buffers

#ifdef DEBUG_EEPROM_READ_WRITE_PAIRING
    DebugSwSer.println("Writing string= ");
    DebugSwSer.print("EEPROM_writeBufferString[inverterNumber]: ");
    DebugSwSer.println(EEPROM_writeBufferString[inverterNumber]);
    DebugSwSer.print("InvSerial: ");
    DebugSwSer.println(InvSerial);
    DebugSwSer.print("InverterShortId: ");
    DebugSwSer.println(InverterShortId);
    DebugSwSer.print("EcuId: ");
    DebugSwSer.println(EcuId);
    DebugSwSer.print("Writing string at EEPROM_SAFE_ADRESS + (40 * inverterNumber): ");
    DebugSwSer.println(EEPROM_SAFE_ADRESS + (40 * inverterNumber));
    DebugSwSer.flush();
#endif
#ifdef DEBUG_OPERATION
    DebugSwSer.print("Saved in EEPROM: ");
    DebugSwSer.print(EEPROM_writeBufferString[inverterNumber]);
    DebugSwSer.print(" at adress: ");
    DebugSwSer.println(EEPROM_SAFE_ADRESS + (40 * inverterNumber));
    DebugSwSer.flush();
    delayMicroseconds(250);
#endif
    _got_inverter_short_number = false;
    _loopcounterPairing = 0; // we should start at command 1 (array 0) again after we successfully paired 1 inverter
    if (_tryingToPairInverterNumber < YC600_MAX_NUMBER_OF_INVERTERS || _tryingToPairInverterNumber < _numberOfInverters)
    {
        _tryingToPairInverterNumber++;
    }
    if (_tryingToPairInverterNumber == _numberOfInverters)
    {
        return 0;
    }

    return 1;
}

// in EEPROM we save at first the full INV_SNS then the string SHORT_ID and after this a String "PAIRED"
// at first we compare the saved EEPROM INV_SNS if it matches the given SNS from the begin function
// if it matches we compare the SHORT_ID to not be 0x0000 or 0xFFFF
// if this matches too we compare the last string "PAIRED"
// if all of this is okay we return a 1 otherwise a 0
// if one of the above doesn't match we delete the last string "PAIRED" and start pairing again
// the "PAIRED" string could be unnecessary but I leave it there for the first time
// Reads out EEPROM, compares EEPROM to given EcuId and InvSns
// if both match copy the EEPROM values in the "underscore" variables
bool YC600::_getPairedIndividualInverter(uint8_t inverterNumber, char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20], char EcuId[])
{
    _local_paired = 0;

#ifdef DEBUG_EEPROM_READ_WRITE_PAIRING
    DebugSwSer.print("++++ _getPairedIndividualInverter: ");
    DebugSwSer.print("should be everytime 0 _local_paired = ");
    DebugSwSer.println(_local_paired);
    DebugSwSer.flush();
#endif

    // Read data from flash memory (EEPROM library)
    String output_string = _readString(EEPROM_SAFE_ADRESS + (40 * inverterNumber));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncpy(EEPROM_inv_sns_out[inverterNumber], output_string.c_str(), 12);
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncpy(EEPROM_inv_ids_out[inverterNumber], output_string.c_str() + 12, 4);
    delayMicroseconds(250);                                                    //give memset a little bit of time to empty all the buffers
    memset(&EEPROM_buffer_paired_out[0], 0, sizeof(EEPROM_buffer_paired_out)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                                    //give memset a little bit of time to empty all the buffers
    strncpy(EEPROM_buffer_paired_out[inverterNumber], output_string.c_str() + 16, 6);
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncpy(EEPROM_buffer_EcuId_out[inverterNumber], output_string.c_str() + 22, 13);
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

    if (strcmp(EEPROM_inv_sns_out[inverterNumber], InvSns[inverterNumber]) == 0)
    {
        _local_paired++;
    }
    if ((strcmp(EEPROM_inv_ids_out[inverterNumber], "0000") != 0) && (strcmp(EEPROM_inv_ids_out[inverterNumber], "FFFF") != 0))
    {
        _local_paired++;
    }
    if (strcmp(EEPROM_buffer_paired_out[inverterNumber], "PAIRED") == 0)
    {
        _local_paired++;
    }
    if (strcmp(EEPROM_buffer_EcuId_out[inverterNumber], EcuId) == 0)
    {
        _local_paired++;
    }

#ifdef DEBUG_EEPROM_READ_WRITE_PAIRING
    DebugSwSer.print("Reading string from EEPROM: ");
    DebugSwSer.println(output_string);
    DebugSwSer.print("*** inverterNumber = ");
    DebugSwSer.println(inverterNumber);
    DebugSwSer.print("*** EEPROM_SAFE_ADRESS + (40 * inverterNumber) = ");
    DebugSwSer.println(EEPROM_SAFE_ADRESS + (40 * inverterNumber));
    DebugSwSer.print(" => EEPROM_inv_sns_out[inverterNumber] content at inverterNumber = ");
    DebugSwSer.println(EEPROM_inv_sns_out[inverterNumber]);
    DebugSwSer.print("  EEPROM_inv_ids_out[inverterNumber] = ");
    DebugSwSer.println(EEPROM_inv_ids_out[inverterNumber]);
    DebugSwSer.print("  EEPROM_buffer_paired_out[inverterNumber] = ");
    DebugSwSer.println(EEPROM_buffer_paired_out[inverterNumber]);
    DebugSwSer.print("  EEPROM_buffer_EcuId_out[inverterNumber] = ");
    DebugSwSer.println(EEPROM_buffer_EcuId_out[inverterNumber]);
    DebugSwSer.println();
    DebugSwSer.println();

    DebugSwSer.print("  _local_paired = ");
    DebugSwSer.println(_local_paired);
    DebugSwSer.print("  _numberOfInverters = ");
    DebugSwSer.println(_numberOfInverters);
    DebugSwSer.print("  strcmp(EEPROM_inv_sns_out[inverterNumber], InvSns[inverterNumber] = ");
    DebugSwSer.println(strcmp(EEPROM_inv_sns_out[inverterNumber], InvSns[inverterNumber]));
    DebugSwSer.print("  (strcmp(EEPROM_inv_ids_out[inverterNumber], 0000)  = ");
    DebugSwSer.println(strcmp(EEPROM_inv_ids_out[inverterNumber], "0000"));
    DebugSwSer.print("  strcmp(EEPROM_inv_ids_out[inverterNumber], FFFF) = ");
    DebugSwSer.println(strcmp(EEPROM_inv_ids_out[inverterNumber], "FFFF"));
    DebugSwSer.print("  strcmp(EEPROM_buffer_paired_out[inverterNumber], PAIRED) = ");
    DebugSwSer.println(strcmp(EEPROM_buffer_paired_out[inverterNumber], "PAIRED"));
    DebugSwSer.print("  strcmp(EEPROM_buffer_EcuId_out[inverterNumber], EcuId) = ");
    DebugSwSer.println(strcmp(EEPROM_buffer_EcuId_out[inverterNumber], EcuId));
    DebugSwSer.flush();
#endif

    if (_local_paired == 4) //only if EEPROM saved values matches the given values from the outside use EEPROM values for each calculation
    {
        strncpy(_InvSns[inverterNumber], EEPROM_inv_sns_out[inverterNumber], strlen(EEPROM_inv_sns_out[inverterNumber]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncpy(_InvIds[inverterNumber], EEPROM_inv_ids_out[inverterNumber], strlen(EEPROM_inv_ids_out[inverterNumber]));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        return true;
    }
    else
    {
        return false;
    }
}

void YC600::_pairInverter(uint8_t inverterNumber, char InvSns[YC600_MAX_NUMBER_OF_INVERTERS][20])
{
    _actualState = 30; //we try to pair the actual inverter
#ifdef DEBUG_OPERATION
    DebugSwSer.print("trying to pair Inverter: ");
    DebugSwSer.print(inverterNumber + 1);
    DebugSwSer.print(" with serial number: ");
    DebugSwSer.println(InvSns[inverterNumber]);
    DebugSwSer.flush();
    delayMicroseconds(250);
#endif
    memset(&_tempSearchInverterLongIdBuffer[0], 0, sizeof(_tempSearchInverterLongIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                                                  //give memset a little bit of time to empty all the buffers
    strncpy(_tempSearchInverterLongIdBuffer, InvSns[inverterNumber], 12);                    //copy the found short ID into an temporary Buffer
    delayMicroseconds(250);                                                                  //give memset a little bit of time to empty all the buffers
#ifdef DEBUG_PAIRING
    DebugSwSer.println();
    DebugSwSer.println("++++ DEBUG_PAIRING");
    DebugSwSer.print("_loopcounterPairing: ");
    DebugSwSer.println(_loopcounterPairing);
    DebugSwSer.print("inverterNumber: ");
    DebugSwSer.println(inverterNumber);
    DebugSwSer.print("InvSns[inverterNumber]: ");
    DebugSwSer.println(InvSns[inverterNumber]);
    DebugSwSer.print("_InvSns[inverterNumber]: ");
    DebugSwSer.println(_InvSns[inverterNumber]);
    DebugSwSer.print("localPaired: ");
    DebugSwSer.println(localPaired);
    DebugSwSer.print("_serial_available(): ");
    DebugSwSer.println(_serial_available());
    DebugSwSer.println();

    DebugSwSer.print("_reset_done: ");
    DebugSwSer.println(_reset_done);
    DebugSwSer.print("_ready: ");
    DebugSwSer.println(_ready);
    DebugSwSer.print("_serial_available(): ");
    DebugSwSer.println(_serial_available());
    DebugSwSer.println();

    DebugSwSer.flush();
#endif

    if (_loopcounterPairing == 0)
    {
        _serial_read(); //only send if the serial buffer is empty
    }
    if (!_serial_available())
    {

        if (_loopcounterPairing <= _calculateNumberOfPairingCommands() - 1 && _reset_done == true && _ready == true) //send all commands one after the other in the right order
        {
#ifdef DEBUG_PAIRING
            DebugSwSer.println();
            DebugSwSer.print("_calculateNumberOfPairingCommands(): ");
            DebugSwSer.println(_calculateNumberOfPairingCommands());
            DebugSwSer.println();
            DebugSwSer.flush();
#endif

            _serial_print(pairingOperationInverterCommands[inverterNumber][_loopcounterPairing]); //TODO use _calculateNumberOfCommands function
        }
        if (_loopcounterPairing < 50) //just to make sure it never overruns
        {
            _loopcounterPairing++;
        }
        if (_loopcounterPairing == _calculateNumberOfPairingCommands())
        {
            _pairing_done = true;
        }
        else if (_loopcounterPairing == _calculateNumberOfPairingCommands() + 2)
        {
            _loopcounterPairing = 0; // start again we should start at command 1 (array 0) again after we successfully paired 1 inverter
        }
    }
}

char *YC600::_split(char *str, const char *delim)
{
    char *p = strstr(str, delim);

    if (p == NULL)
        return NULL; // delimiter not found

    *p = '\0';                // terminate string after head
    return p + strlen(delim); // return tail substring
}

void YC600::_reset()
{
    _error = 0;
    _dirty = true;
    _ready = false;

    _paired = false;
    _ready_to_send = true;
    _waiting_for_response = false;
    _message_decoded = false;
    _reset_done = false;
    _got_inverter_short_number = false;
    _pairing_done = false;

    _init_EEPROM_done = false;

    _numberOfInverters = 0;
    _numberOfPairedInverters = 0;
    _tryingToPairInverterNumber = 0;
    _loopcounterPairing = 0;

    _local_paired = 0;
    _actualState = -1;
    //for the search of the inverter ID matching
    _YC600_index = -1;
    _local_saved = 0;
}