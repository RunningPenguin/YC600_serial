// Please read the README from GitLab because you have to
// flash the CC2530 which you have to use together with this library
// with a custom firmware!

#include <Arduino.h>

// the library uses HW serial in this example so we use SW serial for the output of the inverters to our PC
#include <SoftwareSerial.h>
SoftwareSerial DebugSwSer(14, 12); //Define hardware connections serial console for debuggin reasons to read the actual values

// has to be before the include of YC600_serial.h so it overrides the default of "5" in YC600_serial.h
#define YC600_MAX_NUMBER_OF_INVERTERS 5

// if you encounter issues which could be corrected by multiple resets please increase this value #define has to be before YC600_serial.h
#define CC2530_STARTUP_RESET_TIMES 2

#include "YC600_serial.h"
YC600 myYC600;

// The fake ID which our ESP8266 ECU should use for the binding and communication with the inverters
char ecu_id[] = "D8A3011B9780";

//The number of the inversters is calculated from the array size
//all commands are calculated on itself
//TODO pairing commands retrieve the inverter short ID by itself after successfull pairing
//up to 5 inverters could be used but don't have to be used there could be blank inv_sns too
char inv_sns[YC600_MAX_NUMBER_OF_INVERTERS][20] =
    {
      "408000118123", //Serial number of first inverter
      "408000119347",  //Serial number of second inverter
      "408000118165",  //Serial number of third inverter
      "408000119352",  //Serial number of fourth inverter
      "408000118124"  //Serial number of fifth inverter
};

uint8_t numberOfDefinedInverters = 0;
uint8_t numberOfSavedInverters = 0;
void setup()
{
  DebugSwSer.begin(115200); //Initialize software serial with baudrate of 115200
  while (!DebugSwSer)
    ;
  DebugSwSer.println();
  DebugSwSer.println();
  DebugSwSer.println("**** Setup: initializing SW Serial outside ...");
  DebugSwSer.println();
  DebugSwSer.flush();
  
  // Initialize YC600
  myYC600.setRxTx(13, 15);        //3==HW-RX 1==HW-TX to USB debugging //13==HW-RX alt 15==HW-TX alt to CC2530 // we have to use "Serial.begin" even before the YC600 libs tries to begin
  myYC600.begin(ecu_id, inv_sns); // will initialize HW-serial to 115200 bps
  
  //returns the number of inverters which are saved witht the string "PAIRED" in EEPROM
  numberOfSavedInverters = myYC600.getNumberOfSavedInverters(YC600_MAX_NUMBER_OF_INVERTERS);

  numberOfDefinedInverters = myYC600.calculateNumberOfInverters(inv_sns);
#ifdef OUTER_DEBUG
  DebugSwSer.print("**** Setup: numberOfSavedInverters: ");
  DebugSwSer.print(numberOfSavedInverters);
  DebugSwSer.println();
#endif
}

// == -1 we didn't found an inverter short ID by auto pair routine
// == 10 reset of CC2530 detected
// == 12 all inverters are paired at startup
// == 13 not all inverters are paired at startup - need to pair
// == 18 we received a new string from the CC2530 - one or more unknown strings
// == 20 we try to decode a new message
// == 22 we successfully decoded a new message
// == 30 we try to pair the actual inverter
// == 34 we found an inverter short ID by auto pair routine
// == 38 auto pairing is saving actual informations to EEPROM
// == 40 auto pairing successfull saved actual informations to EEPROM
// == 42 after auto pairing paired all inverters
// possible values of myYC600.getState()

// == -1 if no data was extracted since startup yet
// == 0 for the first inverter from the inv_sns struct you gave to the begin function
// == 1 for the second inverter from the inv_sns struct you gave to the begin function
// == 2 ... and so on
// should change every 5s (or if changed every YC600_SYNC_INTERVAL)
// so if you have one inverter you should get new values from this one every 5s
// if you have 2 inverters you get from each individual inverter updates every 10s
// if you have 3 inverters each inverter updates at 15s
// this has the reason that the polling from the Wemos D1 mini pro to the CC2530
// and then over the air (ZigBee) to the YC600 and back takes some time
// and we could only poll inverter at a time
// possible values and update rate of myYC600.getUpdatedInverterNumber()

int8_t actualState = 0;
int8_t oldState = 0;

int8_t actualInverterNumber = -1;
int8_t oldInverterNumber = -1;

void loop()
{
  myYC600.tick(); //tick function should be called in every loop cycle

  actualState = myYC600.getState();
  if (actualState != oldState)
  {
    oldState = actualState;
    DebugSwSer.print("**** actualState = ");
    switch (actualState)
    {
    case 10:
    {
      DebugSwSer.print("reset of CC2530");
      break;
    }
    case 12:
    {
      DebugSwSer.print("all inverters are paired at startup");
      break;
    }
    case 13:
    {
      DebugSwSer.print("not all inverters are paired at startup - need to pair");
      break;
    }
    case 18:
    {
      DebugSwSer.print("we received a new string from the CC2530 - one or more unknown strings");
      break;
    }
      // not working      case 20:
      // not working      {
      // not working        DebugSwSer.print("we try to decode a new message");
      // not working        break;
      // not working      }
    case 22:
    {
      DebugSwSer.print("we successfully decoded a new message");
      break;
    }
    case 30:
    {
      DebugSwSer.print("auto pairing - we try to pair one inverter");
      break;
    }
      // not working      case 34:
      // not working      {
      // not working        DebugSwSer.print("we found an inverter short ID by auto pair routine");
      // not working        break;
      // not working      }
      // not working      case 38:
      // not working      {
      // not working        DebugSwSer.print("auto pairing is saving actual informations to EEPROM");
      // not working        break;
      // not working      }
    case 40:
    {
      DebugSwSer.print("auto pairing - successfull saved inverter serial number, inverter short ID and ECU-ID to EEPROM");
      break;
    }
    case 42:
    {
      DebugSwSer.print("auto pairing - paired all inverters successfull");
      break;
    }
    default:
      break;
    }
    DebugSwSer.println();
    DebugSwSer.flush();
  }

  actualInverterNumber = myYC600.getUpdatedInverterNumber();

  if (actualInverterNumber != oldInverterNumber)
  {
    oldInverterNumber = actualInverterNumber;
    DebugSwSer.print("**** InverterNumber = ");
    DebugSwSer.print(actualInverterNumber);
    DebugSwSer.println();
    DebugSwSer.print("Decode: frequency: ");
    DebugSwSer.print(myYC600.getAcFrequency(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: temperature: ");
    DebugSwSer.print(myYC600.getInverterTemperature(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: AC voltage: ");
    DebugSwSer.print(myYC600.getAcVoltage(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: current Panel1: ");
    DebugSwSer.print(myYC600.getDcCurrentPanel1(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: current Panel2: ");
    DebugSwSer.print(myYC600.getDcCurrentPanel2(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: voltage Panel1: ");
    DebugSwSer.print(myYC600.getDcVoltagePanel1(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: voltage Panel2: ");
    DebugSwSer.print(myYC600.getDcVoltagePanel2(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: power Panel1: ");
    DebugSwSer.print(myYC600.getDcPowerPanel1(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: power Panel2: ");
    DebugSwSer.print(myYC600.getDcPowerPanel2(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: energy Panel1: ");
    DebugSwSer.print(myYC600.getDcEnergyPanel1(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.print("Decode: energy Panel2: ");
    DebugSwSer.print(myYC600.getDcEnergyPanel2(actualInverterNumber));
    DebugSwSer.println();
    DebugSwSer.println();
    DebugSwSer.flush();
  }
}
